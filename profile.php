<?php
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);

	require 'includes/classes/hydra_file_framework.class.inc';
	require 'includes/classes/hydra_user.class.inc';

	$download = new hydra_file_framework;
	$user = new User;
	$edit_file_info = FALSE;

	if(!$user->is_loggedin()) { header('Location: /'); }


	if(isset($_POST['action_performed'])) {

		$action = htmlentities(strip_tags($_POST['user_action']));
		$user_action_file_id = intval($_POST['file_id']);

		if(in_array($action, array('download', 'view', 'edit', 'delete'))) {

			switch ($action) {
				case 'download':
					$download->FILE($_POST['file_id']);
				break;

				case 'view':
					$download->view_file($_POST['file_id']);
				break;

				case 'edit':
					$edit_file_info = TRUE;
				break;

				case 'delete':
					$delete_status = $download->delete_file($user_action_file_id);
				break;
				
				default:
					echo "<p class='error'>Something went wrong!</p>";
				break;
			}

		}
	}


	if(isset($_POST['downloaded'])) {
		$download->FILE($_POST['file_id']);
	}

	if(isset($_POST['searched'])) {
		
		require 'includes/php/config.inc';

		$query = strtolower(htmlentities(strip_tags($_POST['filename_search'])));

		# preg_replace(patterns, replacement, string)

	    $query = preg_replace('/[^a-z0-9_\s-]/', '', $query);
	    $query = preg_replace('/[\s-]+/', ' ', $query);
	    $query = preg_replace('/[\s_]/', '-', $query);

		header('Location: ' . BASE_URL . 'search/' . $query);
	}

	require 'includes/php/functions.php';
	require 'includes/php/config.inc';

	if(isset($_GET['profile_page'])) {
		$p = $_GET['profile_page'];
	}else{
		$p = NULL;
	}

	switch ($p) {
		case 'view_suggestions':
			$page = 'admin_suggestion_page.inc.php';
			$title = '(ADMIN) View Suggestions';
		break;

		case 'user_management':
			$page = 'admin_usermngmnt_page.inc.php';
			$title = '(ADMIN) User Management';
		break;

		case 'file_management':
			$page = 'admin_filemngmnt_page.inc.php';
			$title = '(ADMIN) File Management';
		break;

		case 'upload':
			$page = 'profile_upload_page.inc.php';
			$title = 'Upload File';
		break;

		case 'inbox':
			$page = 'profile_inbox_page.inc.php';
			$title = 'Inbox';
		break;

		case 'friend_requests':
			$page = 'profile_friendrequest_page.inc.php';
			$title = 'Requests';
		break;

		case 'friends':
			$page = 'profile_friends_page.inc.php';
			$title = 'My Friends';
		break;

		case 'apps':
			$page = 'profile_apps_file.inc.php';
			$title = 'Applications';
		break;

		case 'archives':
			$page = 'profile_archives_file.inc.php';
			$title = 'Archives';
		break;

		case 'docs':
			$page = 'profile_docs_file.inc.php';
			$title = 'Documents';
		break;

		case 'image':
			$page = 'profile_images_file.inc.php';
			$title = 'Images';
		break;

		case 'music':
			$page = 'profile_music_file.inc.php';
			$title = 'Music';
		break;

		case 'video':
			$page = 'profile_video_file.inc.php';
			$title = 'Video';
		break;

		case 'uncategorized':
			$page = 'profile_nocategory_file.inc.php';
			$title = 'Uncategorized';
		break;

		case 'change_pic':
			$page = 'profile_changepic_page.inc.php';
			$title = 'Change Picture';
		break;

		case 'change_pass':
			$page = 'profile_changepass_page.inc.php';
			$title = 'Change Password';
		break;

		case 'update_profile':
			$page = 'profile_updateinfo_page.inc.php';
			$title = 'Update Profile';
		break;

		case 'user':
			$page = 'profile_userview_page.inc.php';
			$title = 'User';
		break;

		default:
			$page = 'profile_main_page.inc.php';
			$title = 'uDrive!';
		break;
	}

	require 'includes/pages/loggedin_header.html';
	require 'includes/pages/' . $page;
	require 'includes/pages/loggedin_footer.html';
?>