<?php
	session_start();
	error_reporting(0);

	require 'includes/classes/hydra_file_framework.class.inc';
	require 'includes/classes/hydra_user.class.inc';
	require 'includes/php/config.inc';
	require DB;

	$file = new hydra_file_framework;
	$user = new User;

	if(isset($_POST['file_download'])) {
		$file->FILE($_POST['file_id']);
	}

	if(isset($_POST['comment_submitted'])) {
		$comment = htmlentities(strip_tags($_POST['comment']));
		$file_id = htmlentities(strip_tags(intval($_POST['file_id'])));

		if(!empty($comment)) {
			if(strlen($comment) >= 3) {
				if($file_id !== 0) {

					$sql = 'INSERT INTO `comments` (`comment_id`, `file_id`, `user_id`, `comment`, `datetime`) VALUES (NULL, "' . $dbc->escape_string($file_id) . '", "' . $_SESSION['user_id'] . '", "' . $dbc->escape_string($comment) . '", NOW())';
					$dbc->query($sql);

				}else { $comment_error_msg = "<p class='error'>Invalid File ID</p>"; }
			}else{ $comment_error_msg = "<p class='error'>Is that a comment?</p>"; }
		}else {
			$comment_error_msg = "<p class='error'>Comment is empty.</p>";
		}

	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>View File</title>
		<meta charset='utf-8' />
		<link rel='stylesheet' type='text/css' href='<?php echo BASE_URL;?>includes/css/styles.css'>
		<script src='<?php echo BASE_URL;?>includes/js/jquery.js'></script>
		<script src='<?php echo BASE_URL;?>includes/js/functions.js'></script>
	</head>
	
	<body>
		<div id='view_file_logo'>
			<a href='/'> <img src='<?php echo BASE_URL;?>images/resource/logov1.png' /> </a>
		</div>
<?php
	$id = htmlentities(strip_tags($_GET['file_id']));

	$sql = '
		SELECT `files`.*, `users`.`firstname`, `users`.`lastname`
		FROM `files` JOIN `users`
		ON `files`.`user_id` = `users`.`user_id`
		WHERE `files`.`file_id` = ? AND `files`.`file_accessibility` = 1
	';

	if($query_file_stmt = $dbc->prepare($sql)) {
		$query_file_stmt->bind_param('i', $id);
		$query_file_stmt->execute();
	}

	$query_file_stmt->store_result();

	if($num_rows = $query_file_stmt->num_rows) {
		$query_file_stmt->bind_result($db_file_id, $user_id, $name, $desc, $bin, $size, $accessibility, $category, $type, $downloaded, $date_uploaded, $firstname, $lastname);
		$query_file_stmt->fetch();
?>
		<div id='suggestions_div'>
			<div class='form'>
				<h3>Send us your suggestions</h3>
				<hr />

				<table>
					<form action='<?php echo BASE_URL;?>includes/php/functions.php' method='POST' id='suggestion_form'>
						<tr>
							<td>Name</td>
							<td> <input type='text' id='fullname'/> </td>
						</tr>

						<tr>
							<td>Email</td>
							<td> <input type='text' id='email'/> </td>
						</tr>

						<tr>
							<td colspan='2'> <textarea id='suggestion' placeholder='Your suggestion here...'></textarea> </td>
						</tr>

						<tr>
							<input type='hidden' name='file_id' id='sugg_file_id' value='<?php echo $db_file_id;?>' />
							<td colspan='2'> <button>Send</button> </td>
						</tr>
					</form>
				</table>
			</div>

			<div id='suggestion_msg'></div>

			<a href='#' class='close'>close</a>
			<div id='clear'></div>
		</div>

		<div id='view_file_div'>
			<a id='suggestions_link' href='#'>Any suggestions?</a>
			<div id='clear'></div>

			<div class='file_category_image'> <img src='<?php echo BASE_URL;?>images/resource/<?php echo $file->file_icon($category);?>' /> </div>
			
			<table>
				<tr>
					<td> <span class='title'>File name: </span> <?php echo $name;?> </td>
				</tr>

				<tr>
					<td> <span class='title'>File type: </span> <?php echo $type , ' FILE';?> </td>
				</tr>

				<tr>
					<td> <span class='title'>File Size: </span> <?php echo $size;?> </td>
				</tr>

				<tr>
					<td> <span class='title'>Uploader: </span> <?php echo $firstname, ' ', $lastname;?> </td>
				</tr>

				<tr>
					<td> <span class='title'>Date uploaded: </span> <?php echo $date_uploaded;?> </td>
				</tr>

				<tr>
					<td> <span class='title'>Downloaded: </span> <?php echo $downloaded;?> </td>
				</tr>

				<tr>
					<td>
						<span class='title'>Description: </span>
						<?php
							if(!is_null($desc)) {
								echo $desc;
							}else{
								echo "<p style='font-style: italic; display: inline; color: #8C8C8C;'>No description.</p>";
							}
						?>
					</td>
				</tr>

				<tr>
					<td style='text-align: center;'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='<?php echo $db_file_id;?>' />
							<input type='hidden' name='file_download' value='true' />
							<button class='download_button'>DOWNLOAD</button>
						</form>
					</td>
				</tr>
			</table>

			<div id='clear'></div>

			<hr />

<?php
	if($user->is_loggedin()) {
?>
			<div id='post_comment_div'>
				<img src='<?php echo BASE_URL;?>images/users/<?php echo $_SESSION['picture'];?>' />
				<form action='' method='POST'>
					<input type='hidden' name='comment_submitted' value='true' />
					<input type='hidden' name='file_id' value='<?php echo $db_file_id;?>' />
					<textarea name='comment' placeholder='Your comment here...'></textarea>
					<button class='post_comment_button'>Comment</button>
				</form>
				<div id='clear'></div>
				<?php echo isset($comment_error_msg) ? $comment_error_msg : ''; ?>
			</div>
<?php
	}
?>



<?php

	$fetch_comments_sql = "
		SELECT CONCAT(`users`.`firstname`, ' ', `users`.`lastname`) AS fullname, `users`.`picture`, `comments`.`comment`
		FROM `users` JOIN `comments`
		ON `users`.`user_id` = `comments`.`user_id`
		WHERE `comments`.`file_id` = $db_file_id
		ORDER BY `comments`.`datetime` DESC
	";

	$fetch_comments_query = $dbc->query($fetch_comments_sql);

	if($fetch_comments_query->num_rows) {
		while($data = $fetch_comments_query->fetch_object()) {
			$user_fullname = $data->fullname;
			$user_picture = $data->picture;
			$user_comment = $data->comment;
?>

			<div id='comment_area'>
				<div id='user_img_div'>
					<img src='<?php echo BASE_URL;?>images/users/<?php echo $user_picture;?>' />
					<span class='user_fullname'> <?php echo $user_fullname;?> </span>
				</div>

				<p>
					<?php echo $user_comment; ?>
				</p>
				
				<div id='clear'></div>
			</div>

<?php
		}
	}else {
		echo "<p class='error'>No comments yet.</p>";
	}
?>
		</div>

<?php
	}else {
		echo "<p class='error'>File not available.</p>";
	}
?>

	</body>
</html>