CREATE DATABASE `udrive`;

USE `udrive`;

CREATE TABLE `users` (
	`user_id` INT AUTO_INCREMENT,
	`username` VARCHAR(20) NOT NULL,
	`password` VARCHAR(100) NOT NULL,
	`picture` VARCHAR(50) DEFAULT 'default.png',
	`firstname` VARCHAR(20) NOT NULL,
	`lastname` VARCHAR(15) NOT NULL,
	`gender` ENUM('male', 'female') NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`user_type` CHAR(6) DEFAULT 'member',
	PRIMARY KEY(`user_id`)
);

CREATE TABLE `verification` (
	`id` INT AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`question` VARCHAR(100) NOT NULL,
	`answer` VARCHAR(100) NOT NULL,
	PRIMARY KEY(`id`),
	CONSTRAINT `user_fk` FOREIGN KEY(`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `friends` (
	`id` INT AUTO_INCREMENT,
	`userid_x` INT NOT NULL,
	`userid_y` INT NOT NULL,
	`friendship_status` BOOLEAN NOT NULL,
	PRIMARY KEY(`id`),
	CONSTRAINT `friend_fk1` FOREIGN KEY(`userid_x`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `friend_fk2` FOREIGN KEY(`userid_y`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `messages` (
	`message_id` INT AUTO_INCREMENT,
	`sender_id` INT NOT NULL,
	`receiver_id` INT NOT NULL,
	`message` TEXT NOT NULL,
	`datetime` DATETIME NOT NULL,
	PRIMARY KEY(`message_id`),
	CONSTRAINT `sender_fk` FOREIGN KEY(`sender_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `receiver_fk` FOREIGN KEY (`receiver_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `files` (
	`file_id` INT AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`file_name` VARCHAR(100) NOT NULL,
	`file_desc` TEXT,
	`file_bin` VARCHAR(100) NOT NULL,
	`file_size` VARCHAR(10) NOT NULL,
	`file_accessibility` BOOLEAN NOT NULL,
	`file_category` ENUM('music', 'video', 'application', 'document', 'archive', 'image', 'nocategory') NOT NULL,
	`file_type` CHAR(10) NOT NULL,
	`file_downloaded` INT DEFAULT 0,
	`date_uploaded` DATETIME NOT NULL,
	PRIMARY KEY(`file_id`),
	CONSTRAINT `fileOwner_fk` FOREIGN KEY(`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `suggestions` (
	`id` INT AUTO_INCREMENT,
	`file_id` INT NOT NULL,
	`fullname` VARCHAR(40) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`message` TEXT NOT NULL,
	`date_received` DATETIME NOT NULL,
	PRIMARY KEY(`id`),
	CONSTRAINT `suggestion_fk` FOREIGN KEY(`file_id`) REFERENCES `files`(`file_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `comments` (
	`comment_id` INT AUTO_INCREMENT,
	`file_id` INT NOT NULL,
	`user_id` INT NOT NULL,
	`comment` TEXT NOT NULL,
	`datetime` DATETIME NOT NULL,
	PRIMARY KEY(`comment_id`),
	CONSTRAINT `file_fk` FOREIGN KEY(`file_id`) REFERENCES `files`(`file_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `commenter_fk` FOREIGN KEY(`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `blocklist` (
	`blocklist_no` INT AUTO_INCREMENT,
	`blocked_userid` INT NOT NULL,
	`reason` TEXT NOT NULL,
	`date_blocked` DATETIME NOT NULL,
	PRIMARY KEY(`blocklist_no`),
	CONSTRAINT `user_blocked` FOREIGN KEY(`blocked_userid`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
);