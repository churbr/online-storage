<?php
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);

	if(isset($_POST['downloaded'])) {

		require 'includes/classes/hydra_file_framework.class.inc';
		$download = new hydra_file_framework;
		
		$download->FILE($_POST['file_id']);
	}

	if(isset($_POST['searched'])) {
		
		require 'includes/php/config.inc';

		$query = strtolower(htmlentities(strip_tags($_POST['filename_search'])));

		# preg_replace(patterns, replacement, string)

	    $query = preg_replace('/[^a-z0-9_\s-]/', '', $query);
	    $query = preg_replace('/[\s-]+/', ' ', $query);
	    $query = preg_replace('/[\s_]/', '-', $query);

		header('Location: ' . BASE_URL . 'search/' . $query);
	}

	require 'includes/php/functions.php';
	require 'includes/php/config.inc';

	if(isset($_GET['p'])) {
		$p = $_GET['p'];
	}else{
		$p = NULL;
	}

	switch ($p) {
		case 'signup':
			$page = 'signup.inc.php';
			$title = 'Register';
		break;

		case 'search':
			$page = 'search.inc.php';
			$title = 'Search';
		break;

		case 'view':
			$page = 'view.inc.php';
			$title = 'View File';
		break;

		case 'forgotpass':
			$page = 'forgotpass.inc.php';
			$title = 'Recover Password';
		break;
		
		default:
			$page = 'main.inc.php';
			$title = 'Welcome to uDrive!';
		break;
	}

	require 'includes/pages/header.html';
	require 'includes/pages/' . $page;
	require 'includes/pages/footer.html';
?>