<?php
	if(isset($_GET['profile_id'])) {

		$info_user_id = htmlentities(strip_tags($_GET['profile_id']));

		if($user->user_exists($info_user_id)) {
			if($info_user_id!==$_SESSION['user_id']) {

				$sql_user_info = '
					SELECT `users`.`username`, `firstname`, `lastname`, `picture`, `gender`, `email`, COUNT(`files`.`file_name`) AS files_uploaded
					FROM `users` JOIN `files`
					ON `users`.`user_id` = `files`.`user_id`
					WHERE `users`.`user_id` = 
				' . $info_user_id;

				$query_user_info = $dbc->query($sql_user_info);

				$data = $query_user_info->fetch_object();

				$username = $data->username;
				$firstname = $data->firstname;
				$lastname = $data->lastname;
				$fullname = $firstname . ' ' . $lastname;
				$picture = $data->picture;
				$gender = $data->gender;
				$email = $data->email;
				$files_uploaded = $data->files_uploaded;
				$friendship_status = $user->friendship_status($info_user_id); // friends, not confirmed, no record


			    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $fullname);
			    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
			    $name_link = preg_replace('/[\s_]/', '-', $name_link);
			    $name_link = strtolower($name_link);


				if(strcmp($friendship_status, 'friends') === 0) {
?>

<div id='view_userprofile_div'>

	<div class='image_container'>
		<img src='<?php echo BASE_URL . 'images/users/' . $picture;?>' />
		<?php echo $username; ?>

		<input type='hidden' id='profile_userid' value='<?php echo $info_user_id;?>' />
		<ul class='userprofile_options'>
			<li> <a href='#'>Send message</a> </li>
			<li> <a href='#' id='unfriend'>Unfriend</a> </li>
		</ul>

	</div>

	<div class='profile_main_info'>
		<h1> <?php echo $firstname . ' ' . $lastname; ?> </h1> <hr />

		<ul>
			<li> <a href='#' class='view_user_files'>Files</a> </li>
			<li> <a href='#' class='view_user_info'>Basic Info</a> </li>
		</ul>

		<div id='clear'></div>

		<div class='container_div'>
			<table class='user_files_menu'>
				<tr>
					<td> <a href='<?php echo BASE_URL . "user/$info_user_id/$name_link";?>/docs'> <img src='<?php echo BASE_URL;?>images/resource/doc.png' /> Documents </a> </td>
					<td> <a href='<?php echo BASE_URL . "user/$info_user_id/$name_link";?>/image'> <img src='<?php echo BASE_URL;?>images/resource/image.png' /> Images </a> </td>
					<td> <a href='<?php echo BASE_URL . "user/$info_user_id/$name_link";?>/music'> <img src='<?php echo BASE_URL;?>images/resource/music.png' /> Music </a> </td>
					<td> <a href='<?php echo BASE_URL . "user/$info_user_id/$name_link";?>/video'> <img src='<?php echo BASE_URL;?>images/resource/video.png' /> Video </a> </td>
				</tr>

				<tr>
					<td> <a href='<?php echo BASE_URL . "user/$info_user_id/$name_link";?>/uncategorized'> <img src='<?php echo BASE_URL;?>images/resource/nocategory.png' /> Uncategorized </a> </td>
					<td> <a href='<?php echo BASE_URL . "user/$info_user_id/$name_link";?>/apps'> <img src='<?php echo BASE_URL;?>images/resource/app.png' /> Applications </a> </td>
					<td> <a href='<?php echo BASE_URL . "user/$info_user_id/$name_link";?>/archives'> <img src='<?php echo BASE_URL;?>images/resource/archive.png' /> Archives </a> </td>
				</tr>
			</table>

			<div class='user_basic_info'>
				<table>
					<tr>
						<td class='user_info_title'>Gender:</td>
						<td> <?php echo $gender; ?> </td>
					</tr>

					<tr>
						<td class='user_info_title'>Email:</td>
						<td> <?php echo $email; ?> </td>
					</tr>

					<tr>
						<td class='user_info_title'>Uploads:</td>
						<td> <?php echo $files_uploaded . ' files'; ?> </td>
					</tr>
				</table>
			</div>
		</div>	
	</div>

	<div id='clear'></div>

	<div id='user_view_file'>

		<?php

			if(isset($_GET['user_view_file'])) {

				$view_file_category = htmlentities(strip_tags($_GET['user_view_file']));

				switch ($view_file_category) {
					case 'docs':
						require BASE_URI . 'includes/pages/profile_docs_file.inc.php';
					break;

					case 'image':
						require BASE_URI . 'includes/pages/profile_images_file.inc.php';
					break;

					case 'music':
						require BASE_URI . 'includes/pages/profile_music_file.inc.php';
					break;

					case 'video':
						require BASE_URI . 'includes/pages/profile_video_file.inc.php';
					break;

					case 'uncategorized':
						require BASE_URI . 'includes/pages/profile_nocategory_file.inc.php';
					break;

					case 'apps':
						require BASE_URI . 'includes/pages/profile_apps_file.inc.php';
					break;

					case 'archives':
						require BASE_URI . 'includes/pages/profile_archives_file.inc.php';
					break;
					
					default:
						echo $view_file_category;
						echo "<p class='error'>File category is invalid.</p>";
					break;
				}
			}
		?>

	</div>

	<div id='clear'></div>
</div>

<?php
				}else{
?>

<div id='view_userprofile_div'>
	<div class='image_container'>
		<img src='<?php echo BASE_URL . 'images/users/' . $picture;?>' />
		<?php echo $username; ?>
	</div>

	<div class='profile_main_info'>
		<h1>
			<span> <?php echo $firstname . ' ' . $lastname; ?> </span>
			<input type='hidden' id='profile_userid' value='<?php echo $info_user_id;?>' />
			<button id='request_button'>
				<span>
					<?php
						$sql_check_requester = 'SELECT `userid_x`, `userid_y` FROM `friends` WHERE `userid_x` = ' . $_SESSION['user_id'] . ' AND `userid_y` = ' . $info_user_id . ' AND `friendship_status` = 0 OR `userid_x` = ' . $info_user_id . ' AND `userid_y` = ' . $_SESSION['user_id'] . ' AND `friendship_status` = 0';

						$check_requester_query = $dbc->query($sql_check_requester);

						$crq_result = $check_requester_query->fetch_object();

						$userid_x = intval($crq_result->userid_x);
						$userid_y = intval($crq_result->userid_y);

						if(strcmp($friendship_status, 'not_confirmed')===0) {
							if($userid_x === intval($_SESSION['user_id'])) {
								echo 'Cancel request';
							}else {
								echo 'Confirm request';
							}
							
						}else {
							echo 'Send request';
						}
					?>
				</span>
			</button>
		</h1>

		<div id='clear'></div>

		<hr />

		<div class='container_div'>
			<table>
				<tr>
					<td class='user_info_title'>Gender:</td>
					<td> <?php echo $gender; ?> </td>
				</tr>

				<tr>
					<td class='user_info_title'>Uploads:</td>
					<td> <?php echo $files_uploaded . ' files'; ?> </td>
				</tr>
			</table>
		</div>	
	</div>

	<div id='clear'></div>
</div>

<?php
				}

			}else { header('Location: /profile'); }
		}else {
			echo "<p class='error'>No record.</p>";
		}
	}
?>