<?php
	require BASE_URI . 'includes/classes/admin.class.inc';
	$admin_user = new Admin;
?>

<div id='user_management_div'>
	<h2>User Management</h2>

	<hr />

	<form action='' method='POST'>
		<input type='hidden' name='admin_action' value='user_search' />
		<input type='text' name='admin_user_search' placeholder='Admin User Search' />
		<input type='submit' value='Search' />
	</form>

<?php

	if(isset($_POST['confirm_action'])) {
		$action = htmlentities(strip_tags($_POST['confirm_action']));
		$user_id = htmlentities(strip_tags($_POST['user_id']));
		$decision = htmlentities(strip_tags($_POST['decision']));

		if(strcmp($action, 'delete') == 0) {
			if(strcmp($decision, 'yes') == 0 && intval($user_id) > 0) {
				$admin_user->admin_delete_user($user_id);
			}
		}elseif(strcmp($action, 'block') == 0) {
			if(strcmp($decision, 'yes') == 0 && intval($user_id) > 0) {
				$reason_blocked = htmlentities(strip_tags($_POST['reason_blocked']));
				
				if(!empty($reason_blocked)) {
					$admin_user->admin_user_block($user_id, $reason_blocked);
				}else {
					echo "<p class='error'>Please specify the reason why user should be blocked.</p>";
				}
			}
		}else {
			echo "<script>alert('Something went wrong.');</script>";
		}
	}

	if(isset($_POST['admin_action']) && $user->is_admin()) {

		$action = htmlentities(strip_tags($_POST['admin_action']));

		switch ($action) {
			case 'user_search':
				$keyword = htmlentities(strip_tags($_POST['admin_user_search']));

				$admin_user->admin_user_search($keyword);
			break;

			case 'delete_user':
				echo "
					<p><strong>Are you sure you want to delete this user? All of its records will be wipe completely.</strong></p>
					<form action='' method='POST'>
						<input type='radio' name='decision' value='yes' checked /> Yes
						<input type='radio' name='decision' value='no' /> No
						<input type='hidden' name='user_id' value='{$_POST['user_id']}' />
						<input type='hidden' name='confirm_action' value='delete' />
						<p> <input type='submit' value='Confirm'> </p>
					</form>
				";
			break;

			case 'block_user':
				echo "
					<p><strong>Please specify the reason why this user needs to be blocked.</strong></p>
					<form action='' method='POST'>
						<p> <textarea name='reason_blocked'></textarea> </p>
						<input type='radio' name='decision' value='yes' checked /> Yes
						<input type='radio' name='decision' value='no' /> No
						<input type='hidden' name='user_id' value='{$_POST['user_id']}' />
						<input type='hidden' name='confirm_action' value='block' />
						<p> <input type='submit' value='Confirm'> </p>
					</form>
				";
			break;
			
			default:
				echo '<script>alert("Error");</script>';
			break;
		}
	}else {
		echo '<div id="admin_view_userlist">';

		$sql_admin_view_users = 'SELECT `user_id`, `username`, `picture`, `firstname`, `lastname`, `gender`, `email`, `user_type` FROM `users` WHERE `user_id` != ' . $_SESSION['user_id'];
		$query_admin_view_users = $dbc->query($sql_admin_view_users);

		while($user_data = $query_admin_view_users->fetch_object()) {
			$user_id 		= intval($user_data->user_id);
			$username 		= $user_data->username;
			$picture 		= $user_data->picture;
			$firstname 		= ucfirst($user_data->firstname);
			$lastname 		= ucfirst($user_data->lastname);
			$gender 		= $user_data->gender;
			$user_type 		= $user_data->user_type;

?>

			<div class='each_user'>
				<img src='<?php echo BASE_URL . 'images/users/' . $picture;?>' />
				<p>
					<?php
						echo "
							$firstname $lastname ($username) <br />
							$gender <br />
							$user_type <br />
						";
					?>
				</p>

				<form action='' method='POST'>
					<input type='hidden' name='user_id' value='<?php echo $user_id;?>' />
					<select name='admin_action'>
						<option value='block_user'>Block</option>
						<option value='delete_user'>Delete</option>
					</select>
					<input type='submit' value='Go' />
				</form>

				<div id='clear'></div>
			</div>

<?php
		}

		echo '</div>';
	}
?>
</div>