<?php
	if(isset($_GET['user_view_file'])) {
?>

<div id='profile_images_div'>
	<h1> <img src='<?php echo BASE_URL . 'images/resource/image.png'?>' /> <p> Image Files </p> </h1>
	
	<div id='clear'></div> <hr />

	<?php
		$sql_query_images = "SELECT * FROM `files` WHERE `file_category` = 'image' AND `user_id` = $info_user_id";

		$query_images_file = $dbc->query($sql_query_images);

		$icon_path = BASE_URL . 'images/resource/download.png';

		if($query_images_file->num_rows) {

			echo '<table id="images_table_view"> <tr>';

			while($image_file_data = $query_images_file->fetch_object()) {
				$file_id = $image_file_data->file_id;
				$file_bin = $image_file_data->file_bin;
				$file_name = $image_file_data->file_name;
				$file_type = $image_file_data->file_type;
				$file_size = $image_file_data->file_size;
				$date_uploaded = $image_file_data->date_uploaded;
				$file_accessibility = $image_file_data->file_accessibility;
				$file_share_type = (intval($file_accessibility) === 1) ? 'Public' : 'Private';


				if(intval($file_accessibility) === 1) {
?>

					<td>
						<a href='<?php echo BASE_URL . 'files/image/' . $file_bin; ?>' data-lightbox='<?php echo $file_name; ?>' data-title='<?php echo $file_name; ?>'>
							<img src='<?php echo BASE_URL . 'files/image/' . $file_bin; ?>' />
							<p>
								<span class='filename'><?php echo $file_name; ?></span> <br />
								<span class='share'><?php echo strtoupper($file_type) . ' File'; ?></span> <br />
								<span class='size'><?php echo $file_size; ?></span>
							</p>
						</a>
						<div class='image_form'>
							<form action='' method='POST'>
								<input type='hidden' name='file_id' value='<?php echo $file_id; ?>' />
								<input type='hidden' name='downloaded' value='true' />
								<input class='download_button' style="background-image: url('<?php echo BASE_URL;?>images/resource/download.png')" type='submit' value='' />
							</form>
						</div>
						<div id='clear'></div>
					</td>

<?php
				}
			}

			echo '</tr> </table>';
		}else { echo "<p class='error'>User haven't uploaded image files yet.</p>"; }
	?>

	<div id='clear'></div>
</div>

<?php
	}else {
?>

<div id='profile_images_div'>
	<h1> <img src='<?php echo BASE_URL . 'images/resource/image.png'?>' /> <p> Image Files </p> </h1>
	
	<div id='clear'></div> <hr />

	<?php
	
		if(!empty($delete_status)) { echo $delete_status; }

		$sql_query_images = "SELECT * FROM `files` WHERE `file_category` = 'image' AND `user_id` = {$_SESSION['user_id']}";

		$query_images_file = $dbc->query($sql_query_images);

		if($query_images_file->num_rows) {

			if($edit_file_info) {
				if(is_array($download->get_filedata($_POST['file_id']))) {

					$file = $download->get_filedata($_POST['file_id']);

					$filename = $file['file_name'];
					$filedesc = $file['file_desc'];
					$fileshare = intval($file['file_accessibility']);
					$filecategory = $file['file_category'];
?>

		<div id='user_edit_file'>
			<table>
				<thead>
					<th>Filename</th>
					<th>Description</th>
					<th>Shared To</th>
					<th>Move To</th>
				</thead>

				<tr>
					<form id='edit_file_info'>
						<td> <input type='text' id='filename' value='<?php echo $filename; ?>' /> </td>
						<td> <textarea id='filedesc' placeholder='No description...'><?php echo $filedesc;?></textarea> </td>
						<td>
							<select id='sharedto'>
								<option <?php echo ($fileshare === 1) ? 'selected':''; ?> value='1'>Public</option>
								<option <?php echo ($fileshare === 0) ? 'selected':''; ?> value='0'>Only me</option>
							</select>
						</td>
						<td>
							<select id='moveto'>
								<?php
									$file_category_list = array('music', 'video', 'application', 'document', 'archive', 'image', 'nocategory');

									foreach ($file_category_list as $category) {
										if($filecategory == $category) {
											echo "<option selected>$category</option>";
										}else {
											echo "<option>$category</option>";
										}
									}
								?>
							</select>
						</td>
						<input type='hidden' id='id' value='<?php echo $_POST['file_id']; ?>' />
						<td> <input type='submit' value='Done' /> </td>
					</form>
				</tr>
			</table>

			<div id='edit_file_info_msg'></div>

			<a href='#' class='close'>Close</a>

			<div id='clear'></div>
		</div>


<?php

				} else { echo "<p class='error'>File entry doesn't exist.</p>"; }
			}

			echo '<table id="images_table_view"> <tr>';

			while($image_file_data = $query_images_file->fetch_object()) {
				$file_id = $image_file_data->file_id;
				$file_name = $image_file_data->file_name;
				$file_bin = $image_file_data->file_bin;
				$file_size = $image_file_data->file_size;
				$date_uploaded = $image_file_data->date_uploaded;
				$file_accessibility = $image_file_data->file_accessibility;
				$file_share_type = (intval($file_accessibility) === 1) ? 'Public' : 'Private';
?>

			<td>
				<a href='<?php echo BASE_URL . 'files/image/' . $file_bin; ?>' data-lightbox='<?php echo $file_name; ?>' data-title='<?php echo $file_name; ?>'>
					<img src='<?php echo BASE_URL . 'files/image/' . $file_bin; ?>' />
					<p>
						<span class='filename'><?php echo $file_name; ?></span> <br />
						<span class='share'><?php echo $file_share_type; ?></span> <br />
						<span class='size'><?php echo $file_size; ?></span>
					</p>
				</a>
				<div class='image_form'>
					<form action='' method='POST'>
						<input type='hidden' name='file_id' value='<?php echo $file_id; ?>' />
						<select name='user_action'>
							<option value='download'>Download</option>
							<option value='view'>View</option>
							<option value='edit'>Edit</option>
							<option value='delete'>Delete</option>
						</select>
						<input class='go_button' type='submit' name='action_performed' value='GO' />
					</form>
				</div>
				<div id='clear'></div>
			</td>

<?php
			}

			echo '</tr> </table>';
		}else { echo "<p class='error'>You have no image files uploaded yet.</p>"; }
	?>

	<div id='clear'></div>
</div>

<?php
	}
?>