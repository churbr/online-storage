<h4>Friends</h4>

<hr />	

<div id='profile_friends_div'>

<?php
	
	$column = 2;
	$count = 0;

	$sql_view_friends = '
		SELECT DISTINCT users.user_id, picture, firstname, lastname, email, friends.friendship_status
		FROM users JOIN friends
		ON users.user_id = friends.userid_x OR users.user_id = friends.userid_y
		WHERE friends.friendship_status = 1 AND friends.userid_x = ' . $_SESSION['user_id'] . ' OR friends.userid_y = ' . $_SESSION['user_id'];

	$query_view_friends = $dbc->query($sql_view_friends);

	if($query_view_friends->num_rows) {

		echo "<table id='friends_list'>";

		while($friend_data = $query_view_friends->fetch_object()) {

			if($count == 0) { echo '<tr>'; }

			$user_id 			= $friend_data->user_id;
			$picture 			= $friend_data->picture;
			$firstname 			= ucfirst($friend_data->firstname);
			$lastname 			= ucfirst($friend_data->lastname);
			$fullname 			= $firstname . ' ' . $lastname;
			$email 				= $friend_data->email;
			$friendship_status 	= $friend_data->friendship_status;

			$name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $fullname);
			$name_link = preg_replace('/[\s-]+/', ' ', $name_link);
			$name_link = preg_replace('/[\s_]/', '-', $name_link);
			$name_link = strtolower($name_link);

			if($user_id != $_SESSION['user_id'] AND $friendship_status != 0) {

				echo <<<FRIEND
					<td class='$user_id'>
						<a href='images/users/$picture' data-lightbox='$firstname' data-title='$firstname'>
							<img src='images/users/$picture' />
						</a>
						<div class='user_detail'>
							<p>
								<a href='user/$user_id/$name_link'>$fullname</a> <br />
								<span class='count_files'>$email</span>
							</p>
							<button class='$user_id'>Friends</button>
						</div>
					</td>
FRIEND;

				$count++;
			}

			if($count == $column) { echo '</tr>'; }
		}
/*
		if($count < $column) {
			for(; $count<=$column; $column++) {
				echo '<td></td>';
			}
			echo '</tr>';
		}
*/
		echo '</table>';

	}else{
		echo '<div class="correct">You do not have friends yet.</div>';
	}
?>

</div>