<?php ob_start(); ?>

<div id='profile_changepass_div'>

	<h1>Change Password</h1> <hr />

	<form action='' method='POST' id='changepass_form'>
		<input type='hidden' name='changed_password' value='true' />
		<p> Old password <input type='password' name='oldpass' /> </p>
		<p> New password <input type='password' name='newpass' /> </p>
		<p> Confirm Password <input type='password' name='confirmpass' /> </p>
		<p> <input type='submit' class='fp_submit_button' value='Change Password' /> </p>
	</form>


	
	<div id='fp_msg'>

		<?php

			if(isset($_POST['changed_password'])) {

				if(!empty($_POST['oldpass']) && !empty($_POST['newpass']) && !empty($_POST['confirmpass'])) {
		    		$oldpass = htmlentities(strip_tags(addslashes(strtolower($_POST['oldpass']))));
		    		$newpass = htmlentities(strip_tags(addslashes(strtolower($_POST['newpass']))));
		    		$confirmpass = htmlentities(strip_tags(addslashes(strtolower($_POST['confirmpass']))));

		    		if($user->db_password_matched($_SESSION['username'], $oldpass)) {
			    		if($user->check_password($newpass)) {
			    			if(strcmp($newpass, $confirmpass)===0) {

		    					$newpass = md5($newpass);

		    					$statement = $dbc->prepare("UPDATE `users` SET `password` = '$newpass' WHERE `user_id` = ? AND `username` = ?");
		    					$statement->bind_param('is', $_SESSION['user_id'], $_SESSION['username']);
		    					$statement->execute();

		    					if($dbc->affected_rows) {
		    						header('Refresh:5;url=/profile');
		    						echo "<p class='success'>Password successfully updated.</p>";
		    					}else { echo "<p class='error'>The password you set is the same with the old password.</p>"; }

			    			}else { echo "<p class='error'>New passwords doesn't matched.</p>"; }
			       		}else { echo "<p class='error'>Password must be atleast 8 characters long.</p>"; }
		    		}else { echo "<p class='error'>Old password is incorrect.</p>"; }
				}else { echo "<p class='error'>Please complete the fields.</p>"; }
			}

		?>

	</div>
</div>