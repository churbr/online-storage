<?php
	if(isset($_GET['user_view_file'])) {
?>

<div id='profile_adan_div'>
	<h2> <img src='<?php echo BASE_URL . 'images/resource/nocategory.png'?>' /> <p> Uncategorized Files </p> </h2>
	
	<div id='clear'></div> <hr />

	<?php
		$sql_query_uncategorized = "SELECT * FROM `files` WHERE `file_category` = 'nocategory' AND `user_id` = $info_user_id";

		$query_uncategorized_file = $dbc->query($sql_query_uncategorized);

		$icon_path = BASE_URL . 'images/resource/download.png';

		if($query_uncategorized_file->num_rows) {

			echo "
				<table>
					<thead>
						<th>FILENAME</th>
						<th>SIZE</th>
						<th>DATE UPLOADED</th>
						<th colspan='2'></th>
					</thead>
			";


			while($uncategorized_file_data = $query_uncategorized_file->fetch_object()) {
				$file_id = $uncategorized_file_data->file_id;
				$file_name = $uncategorized_file_data->file_name;
				$file_size = $uncategorized_file_data->file_size;
				$date_uploaded = $uncategorized_file_data->date_uploaded;
				$file_accessibility = $uncategorized_file_data->file_accessibility;
				$file_share_type = (intval($file_accessibility) === 1) ? 'Public' : 'Private';

				if(intval($file_accessibility) === 1) {
					echo <<<NOCATEGORY_FILES
						<tr>
							<form action='' method='POST'>
								<input type='hidden' name='file_id' value='$file_id' />
								<input type='hidden' name='downloaded' value='true' />
								<td width='200'> <p class='userview_filename'> $file_name </p> </td>
								<td> <p class='userview_filesize'> $file_size </p> </td>
								<td> <p class='userview_basicfileinfo'> $date_uploaded </p> </td>
								<td> <input class='download_button' style="background-image: url('$icon_path')" type='submit' value='' /> </td>
							</form>
						</tr>
NOCATEGORY_FILES;
				}
			}

			echo '</table>';
		}else { echo "<p class='error'>User haven't uploaded files that is uncategorized yet.</p>"; }
	?>

	<div id='clear'></div>
</div>

<?php
	}else {
?>

<div id='profile_adan_div'>
	<h1> <img src='<?php echo BASE_URL . 'images/resource/nocategory.png'?>' /> <p> Uncategorized Files </p> </h1>
	
	<div id='clear'></div> <hr />

	<?php
	
		if(!empty($delete_status)) { echo $delete_status; }

		$sql_query_uncategorized = "SELECT * FROM `files` WHERE `file_category` = 'nocategory' AND `user_id` = {$_SESSION['user_id']}";

		$query_uncategorized_file = $dbc->query($sql_query_uncategorized);

		if($query_uncategorized_file->num_rows) {

			if($edit_file_info) {
				if(is_array($download->get_filedata($_POST['file_id']))) {

					$file = $download->get_filedata($_POST['file_id']);

					$filename = $file['file_name'];
					$filedesc = $file['file_desc'];
					$fileshare = intval($file['file_accessibility']);
					$filecategory = $file['file_category'];
?>

		<div id='user_edit_file'>
			<table>
				<thead>
					<th>Filename</th>
					<th>Description</th>
					<th>Shared To</th>
					<th>Move To</th>
				</thead>

				<tr>
					<form id='edit_file_info'>
						<td> <input type='text' id='filename' value='<?php echo $filename; ?>' /> </td>
						<td> <textarea id='filedesc' placeholder='No description...'><?php echo $filedesc;?></textarea> </td>
						<td>
							<select id='sharedto'>
								<option <?php echo ($fileshare === 1) ? 'selected':''; ?> value='1'>Public</option>
								<option <?php echo ($fileshare === 0) ? 'selected':''; ?> value='0'>Only me</option>
							</select>
						</td>
						<td>
							<select id='moveto'>
								<?php
									$file_category_list = array('music', 'video', 'application', 'document', 'archive', 'image', 'nocategory');

									foreach ($file_category_list as $category) {
										if($filecategory == $category) {
											echo "<option selected>$category</option>";
										}else {
											echo "<option>$category</option>";
										}
									}
								?>
							</select>
						</td>
						<input type='hidden' id='id' value='<?php echo $_POST['file_id']; ?>' />
						<td> <input type='submit' value='Done' /> </td>
					</form>
				</tr>
			</table>

			<div id='edit_file_info_msg'></div>

			<a href='#' class='close'>Close</a>

			<div id='clear'></div>
		</div>


<?php

				} else { echo "<p class='error'>File entry doesn't exist.</p>"; }
			}


			echo "
				<table>
					<thead>
						<th>FILENAME</th>
						<th>SIZE</th>
						<th>DATE UPLOADED</th>
						<th>SHARE</th>
						<th colspan='2'>ACTION</th>
					</thead>
			";


			while($uncategorized_file_data = $query_uncategorized_file->fetch_object()) {
				$file_id = $uncategorized_file_data->file_id;
				$file_name = $uncategorized_file_data->file_name;
				$file_size = $uncategorized_file_data->file_size;
				$date_uploaded = $uncategorized_file_data->date_uploaded;
				$file_accessibility = $uncategorized_file_data->file_accessibility;
				$file_share_type = (intval($file_accessibility) === 1) ? 'Public' : 'Private';
				
				echo <<<NOCATEGORY_FILES
					<tr>
						<form action='' method='POST'>
							<td width='200'> <p class='userview_filename'> $file_name </p> </td>
							<td> <p class='userview_filesize'> $file_size </p> </td>
							<td> <p class='userview_basicfileinfo'> $date_uploaded </p> </td>
							<td> <p class='userview_basicfileinfo'> $file_share_type </p> </td>
							<td>
								<input type='hidden' name='file_id' value='$file_id' />
								<select name='user_action'>
									<option value='download'>Download</option>
									<option value='view'>View</option>
									<option value='edit'>Edit</option>
									<option value='delete'>Delete</option>
								</select>
							</td>
							<td> <input class='go_button' type='submit' name='action_performed' value='GO' /> </td>
						</form>
					</tr>
NOCATEGORY_FILES;
			}

			echo '</table>';
		}else { echo "<p class='error'>You have no uncategorized files uploaded yet.</p>"; }
	?>

	<div id='clear'></div>
</div>

<?php
	}
?>