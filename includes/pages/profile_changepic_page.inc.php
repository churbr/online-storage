<?php
	ob_start();

	require 'includes/classes/hydra_upload_framework.class.inc';
	$file = new file_upload;
?>

<div id='profile_changepic_div'>
	<h1>Change your picture</h1> <hr />

	<img src='images/users/<?php echo $_SESSION['picture'];?>' />

	<div class='change_pic_form'>
		<form action='' method='POST' enctype='multipart/form-data'>
			<input type='hidden' name='changed_picture' value='true' />
			<input type='file' name='image' />
			<input type='submit' value='Change Picture' />
		</form>

		<?php

			if(isset($_POST['changed_picture'])) {
				if(!empty($_FILES['image']['name'])) {
					if($user->check_image($_FILES['image'])) {
						$image_name = $file->make_random_filename($_FILES['image']['name']);

						$query = $dbc->query("UPDATE `users` SET `picture` = '$image_name' WHERE `user_id` = {$_SESSION['user_id']}");

						if(move_uploaded_file($_FILES['image']['tmp_name'], 'images/users/' . $image_name) && $dbc->affected_rows) {
							
							session_start();
							$_SESSION['picture'] = $image_name;
							header('Refresh:1;url=/change_pic');
							
						}else { echo "<p class='error'>Something went wrong.</p>"; }

					}else { echo "<p class='error'>Invalid format.</p>"; }
				}else { echo "<p class='error'>No file selected.</p>"; }
			}

		?>

	</div>

	<div id='clear'></div>

</div>