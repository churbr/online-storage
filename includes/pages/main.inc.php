<?php
	$sql_app_files = 'SELECT `file_id`, `file_name`, `file_size`, `file_accessibility`, `file_type`, `file_downloaded`, `date_uploaded` FROM `files` WHERE `file_category` = "application" AND `file_accessibility` = 1 ORDER BY `file_downloaded` DESC LIMIT 10';
	$sql_doc_files = 'SELECT `file_id`, `file_name`, `file_size`, `file_accessibility`, `file_type`, `file_downloaded`, `date_uploaded` FROM `files` WHERE `file_category` = "document" AND `file_accessibility` = 1 ORDER BY `file_downloaded` DESC LIMIT 10';
	$sql_image_files = 'SELECT `file_id`, `file_name`, `file_size`, `file_accessibility`, `file_type`, `file_downloaded`, `date_uploaded` FROM `files` WHERE `file_category` = "image" AND `file_accessibility` = 1 ORDER BY `file_downloaded` DESC LIMIT 10';
	$sql_music_files = 'SELECT `file_id`, `file_name`, `file_size`, `file_accessibility`, `file_type`, `file_downloaded`, `date_uploaded` FROM `files` WHERE `file_category` = "music" AND `file_accessibility` = 1 ORDER BY `file_downloaded` DESC LIMIT 10';
	$sql_video_files = 'SELECT `file_id`, `file_name`, `file_size`, `file_accessibility`, `file_type`, `file_downloaded`, `date_uploaded` FROM `files` WHERE `file_category` = "video" AND `file_accessibility` = 1 ORDER BY `file_downloaded` DESC LIMIT 10';
	$sql_archive_files = 'SELECT `file_id`, `file_name`, `file_size`, `file_accessibility`, `file_type`, `file_downloaded`, `date_uploaded` FROM `files` WHERE `file_category` = "archive" AND `file_accessibility` = 1 ORDER BY `file_downloaded` DESC LIMIT 10';
	$sql_nocategory_files = 'SELECT `file_id`, `file_name`, `file_size`, `file_accessibility`, `file_type`, `file_downloaded`, `date_uploaded` FROM `files` WHERE `file_category` = "nocategory" AND `file_accessibility` = 1 ORDER BY `file_downloaded` DESC LIMIT 10';
?>

<div id='top_downloads_div'>

	<div class='top_app'>
		<h2> <img src='images/resource/app.png' /> Applications</h2>

<?php

	$query_app_files = $dbc->query($sql_app_files);

	if($query_app_files->num_rows) {

		echo "
			<table>
				<thead>
					<th>FILENAME</th>
					<th>SIZE</th>
					<th>DOWNLOADED</th>
					<th>DATE</th>
					<th style='border-right: none;'></th>
				</thead>

				<tbody>
		";

		while($app_data = $query_app_files->fetch_object()) {

			$file_id = $app_data->file_id;
			$file_name = $app_data->file_name;
			$file_size = $app_data->file_size;
			$file_type = strtoupper($app_data->file_type);
			$file_downloaded = $app_data->file_downloaded;
			$timestamp = strtotime($app_data->date_uploaded);
			$date_uploaded = date('F d, Y g:i A', $timestamp);

		    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $file_name);
		    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
		    $name_link = preg_replace('/[\s_]/', '-', $name_link);

			echo <<<FILE_LIST
				<tr>
					<td style='max-width: 150px;'> <a class='filename' href='view/$file_id/$name_link'>$file_name (<span class='filetype'>$file_type File</span>)</a> </td>
					<td class='filesize'>$file_size</td>
					<td class='filedownloaded'>$file_downloaded</td>
					<td class='filedate'>$date_uploaded</td>
					<td style='text-align: center; border-right: none;'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='$file_id' />
							<input type='hidden' name='downloaded' value='true' />
							<input class='file_download' type='submit' value='Download' />
						</form>
					</td>
				</tr>
FILE_LIST;
		}

		echo "
				</tbody>
			</table>
		";

	}else {
		echo "<p class='correct'>Users have not uploaded any application files yet.</p>";
	}

?>
	</div>
</div>

<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

<div id='top_downloads_div'>

	<div class='top_doc'>
		<h2> <img src='images/resource/doc.png' /> Documents</h2>

<?php

	$query_doc_files = $dbc->query($sql_doc_files);

	if($query_doc_files->num_rows) {

		echo "
			<table>
				<thead>
					<th>FILENAME</th>
					<th>SIZE</th>
					<th>DOWNLOADED</th>
					<th>DATE</th>
					<th style='border-right: none;'></th>
				</thead>

				<tbody>
		";

		while($doc_data = $query_doc_files->fetch_object()) {

			$file_id = $doc_data->file_id;
			$file_name = $doc_data->file_name;
			$file_size = $doc_data->file_size;
			$file_type = strtoupper($doc_data->file_type);
			$file_downloaded = $doc_data->file_downloaded;
			$timestamp = strtotime($doc_data->date_uploaded);
			$date_uploaded = date('F d, Y g:i A', $timestamp);

		    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $file_name);
		    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
		    $name_link = preg_replace('/[\s_]/', '-', $name_link);

			echo <<<FILE_LIST
				<tr>
					<td style='max-width: 150px;'> <a class='filename' href='view/$file_id/$name_link'>$file_name (<span class='filetype'>$file_type File</span>)</a> </td>
					<td class='filesize'>$file_size</td>
					<td class='filedownloaded'>$file_downloaded</td>
					<td class='filedate'>$date_uploaded</td>
					<td style='text-align: center; border-right: none;'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='$file_id' />
							<input type='hidden' name='downloaded' value='true' />
							<input class='file_download' type='submit' value='Download' />
						</form>
					</td>
				</tr>
FILE_LIST;
		}

		echo "
				</tbody>
			</table>
		";

	}else {
		echo "<p class='correct'>Users have not uploaded any document files yet.</p>";
	}

?>
	</div>
</div>

<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

<div id='top_downloads_div'>

	<div class='top_image'>
		<h2> <img src='images/resource/image.png' /> Images</h2>

<?php

	$query_image_files = $dbc->query($sql_image_files);

	if($query_image_files->num_rows) {

		echo "
			<table>
				<thead>
					<th>FILENAME</th>
					<th>SIZE</th>
					<th>DOWNLOADED</th>
					<th>DATE</th>
					<th style='border-right: none;'></th>
				</thead>

				<tbody>
		";

		while($image_data = $query_image_files->fetch_object()) {

			$file_id = $image_data->file_id;
			$file_name = $image_data->file_name;
			$file_size = $image_data->file_size;
			$file_type = strtoupper($image_data->file_type);
			$file_downloaded = $image_data->file_downloaded;
			$timestamp = strtotime($image_data->date_uploaded);
			$date_uploaded = date('F d, Y g:i A', $timestamp);

		    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $file_name);
		    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
		    $name_link = preg_replace('/[\s_]/', '-', $name_link);

			echo <<<FILE_LIST
				<tr>
					<td style='max-width: 150px;'> <a class='filename' href='view/$file_id/$name_link'>$file_name (<span class='filetype'>$file_type File</span>)</a> </td>
					<td class='filesize'>$file_size</td>
					<td class='filedownloaded'>$file_downloaded</td>
					<td class='filedate'>$date_uploaded</td>
					<td style='text-align: center; border-right: none;'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='$file_id' />
							<input type='hidden' name='downloaded' value='true' />
							<input class='file_download' type='submit' value='Download' />
						</form>
					</td>
				</tr>
FILE_LIST;
		}

		echo "
				</tbody>
			</table>
		";

	}else {
		echo "<p class='correct'>Users have not uploaded any image files yet.</p>";
	}

?>
	</div>
</div>

<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

<div id='top_downloads_div'>

	<div class='top_audio'>
		<h2> <img src='images/resource/music.png' /> Audio</h2>

<?php

	$query_music_files = $dbc->query($sql_music_files);

	if($query_music_files->num_rows) {

		echo "
			<table>
				<thead>
					<th>FILENAME</th>
					<th>SIZE</th>
					<th>DOWNLOADED</th>
					<th>DATE</th>
					<th style='border-right: none;'></th>
				</thead>

				<tbody>
		";

		while($music_data = $query_music_files->fetch_object()) {

			$file_id = $music_data->file_id;
			$file_name = $music_data->file_name;
			$file_size = $music_data->file_size;
			$file_type = strtoupper($music_data->file_type);
			$file_downloaded = $music_data->file_downloaded;
			$timestamp = strtotime($music_data->date_uploaded);
			$date_uploaded = date('F d, Y g:i A', $timestamp);

		    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $file_name);
		    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
		    $name_link = preg_replace('/[\s_]/', '-', $name_link);

			echo <<<FILE_LIST
				<tr>
					<td style='max-width: 150px;'> <a class='filename' href='view/$file_id/$name_link'>$file_name (<span class='filetype'>$file_type File</span>)</a> </td>
					<td class='filesize'>$file_size</td>
					<td class='filedownloaded'>$file_downloaded</td>
					<td class='filedate'>$date_uploaded</td>
					<td style='text-align: center; border-right: none;'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='$file_id' />
							<input type='hidden' name='downloaded' value='true' />
							<input class='file_download' type='submit' value='Download' />
						</form>
					</td>
				</tr>
FILE_LIST;
		}

		echo "
				</tbody>
			</table>
		";

	}else {
		echo "<p class='correct'>Users have not uploaded any audio files yet.</p>";
	}

?>
	</div>
</div>

<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

<div id='top_downloads_div'>

	<div class='top_video'>
		<h2> <img src='images/resource/video.png' /> Video</h2>

<?php

	$query_video_files = $dbc->query($sql_video_files);

	if($query_video_files->num_rows) {

		echo "
			<table>
				<thead>
					<th>FILENAME</th>
					<th>SIZE</th>
					<th>DOWNLOADED</th>
					<th>DATE</th>
					<th style='border-right: none;'></th>
				</thead>

				<tbody>
		";

		while($video_data = $query_video_files->fetch_object()) {

			$file_id = $video_data->file_id;
			$file_name = $video_data->file_name;
			$file_size = $video_data->file_size;
			$file_type = strtoupper($video_data->file_type);
			$file_downloaded = $video_data->file_downloaded;
			$timestamp = strtotime($video_data->date_uploaded);
			$date_uploaded = date('F d, Y g:i A', $timestamp);

		    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $file_name);
		    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
		    $name_link = preg_replace('/[\s_]/', '-', $name_link);

			echo <<<FILE_LIST
				<tr>
					<td style='max-width: 150px;'> <a class='filename' href='view/$file_id/$name_link'>$file_name (<span class='filetype'>$file_type File</span>)</a> </td>
					<td class='filesize'>$file_size</td>
					<td class='filedownloaded'>$file_downloaded</td>
					<td class='filedate'>$date_uploaded</td>
					<td style='text-align: center; border-right: none;'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='$file_id' />
							<input type='hidden' name='downloaded' value='true' />
							<input class='file_download' type='submit' value='Download' />
						</form>
					</td>
				</tr>
FILE_LIST;
		}

		echo "
				</tbody>
			</table>
		";

	}else {
		echo "<p class='correct'>Users have not uploaded any video files yet.</p>";
	}

?>
	</div>
</div>

<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

<div id='top_downloads_div'>

	<div class='top_archive'>
		<h2> <img src='images/resource/archive.png' /> Archive</h2>

<?php

	$query_archive_files = $dbc->query($sql_archive_files);

	if($query_archive_files->num_rows) {

		echo "
			<table>
				<thead>
					<th>FILENAME</th>
					<th>SIZE</th>
					<th>DOWNLOADED</th>
					<th>DATE</th>
					<th style='border-right: none;'></th>
				</thead>

				<tbody>
		";

		while($archive_data = $query_archive_files->fetch_object()) {

			$file_id = $archive_data->file_id;
			$file_name = $archive_data->file_name;
			$file_size = $archive_data->file_size;
			$file_type = strtoupper($archive_data->file_type);
			$file_downloaded = $archive_data->file_downloaded;
			$timestamp = strtotime($archive_data->date_uploaded);
			$date_uploaded = date('F d, Y g:i A', $timestamp);

		    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $file_name);
		    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
		    $name_link = preg_replace('/[\s_]/', '-', $name_link);

			echo <<<FILE_LIST
				<tr>
					<td style='max-width: 150px;'> <a class='filename' href='view/$file_id/$name_link'>$file_name (<span class='filetype'>$file_type File</span>)</a> </td>
					<td class='filesize'>$file_size</td>
					<td class='filedownloaded'>$file_downloaded</td>
					<td class='filedate'>$date_uploaded</td>
					<td style='text-align: center; border-right: none;'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='$file_id' />
							<input type='hidden' name='downloaded' value='true' />
							<input class='file_download' type='submit' value='Download' />
						</form>
					</td>
				</tr>
FILE_LIST;
		}

		echo "
				</tbody>
			</table>
		";

	}else {
		echo "<p class='correct'>Users have not uploaded any archive files yet.</p>";
	}

?>
	</div>
</div>

<!-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->

<div id='top_downloads_div'>

	<div class='top_uncategorized'>
		<h2> <img src='images/resource/nocategory.png' /> Uncategorized</h2>

<?php

	$query_nocategory_files = $dbc->query($sql_nocategory_files);

	if($query_nocategory_files->num_rows) {

		echo "
			<table>
				<thead>
					<th>FILENAME</th>
					<th>SIZE</th>
					<th>DOWNLOADED</th>
					<th>DATE</th>
					<th style='border-right: none;'></th>
				</thead>

				<tbody>
		";

		while($nocategory_data = $query_nocategory_files->fetch_object()) {

			$file_id = $nocategory_data->file_id;
			$file_name = $nocategory_data->file_name;
			$file_size = $nocategory_data->file_size;
			$file_type = strtoupper($nocategory_data->file_type);
			$file_downloaded = $nocategory_data->file_downloaded;
			$timestamp = strtotime($nocategory_data->date_uploaded);
			$date_uploaded = date('F d, Y g:i A', $timestamp);

		    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $file_name);
		    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
		    $name_link = preg_replace('/[\s_]/', '-', $name_link);

			echo <<<FILE_LIST
				<tr>
					<td style='max-width: 150px;'> <a class='filename' href='view/$file_id/$name_link'>$file_name (<span class='filetype'>$file_type File</span>)</a> </td>
					<td class='filesize'>$file_size</td>
					<td class='filedownloaded'>$file_downloaded</td>
					<td class='filedate'>$date_uploaded</td>
					<td style='text-align: center; border-right: none;'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='$file_id' />
							<input type='hidden' name='downloaded' value='true' />
							<input class='file_download' type='submit' value='Download' />
						</form>
					</td>
				</tr>
FILE_LIST;
		}

		echo "
				</tbody>
			</table>
		";

	}else {
		echo "<p class='correct'>Users have not uploaded any uncategorized files yet.</p>";
	}

?>
	</div>
</div>