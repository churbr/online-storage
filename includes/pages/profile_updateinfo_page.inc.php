<?php
	ob_start();

	$info_sql = "
		SELECT `users`.`firstname`, `lastname`, `gender`, `email`, `verification`.`question`, `answer`
		FROM `users` JOIN `verification`
		USING (`user_id`)
		WHERE `user_id` = {$_SESSION['user_id']}
	";

	$query = $dbc->query($info_sql);

	$info = $query->fetch_object();

	$db_firstname = $info->firstname;
	$db_lastname = $info->lastname;
	$db_gender = $info->gender;
	$db_email = $info->email;
	$db_question = $info->question;
	$db_answer = $info->answer;
?>

<div id='profile_updateinfo_div'>
	<h1>Edit Profile</h1> <hr />


	<form action='' method='POST'>
		<table>
			<tr>
				<td>Firstname</td>
				<td>Lastname</td>
			</tr>

			<tr>
				<td> <input type='text' name='firstname' value='<?php echo $db_firstname;?>' required /> </td>
				<td> <input type='text' name='lastname' value='<?php echo $db_lastname;?>' required /> </td>
			</tr>

			<tr>
				<td>Gender</td>
				<td>
					<input type='radio' name='gender' value='male' <?php echo ($db_gender == 'male') ? 'checked':null;?> /> Male
					<input type='radio' name='gender' value='female' <?php echo ($db_gender == 'female') ? 'checked':null;?> /> Female
				</td>
			</tr>

			<tr>
				<td>Email</td>
				<td> <input type='text' name='email' value='<?php echo $db_email;?>' required /> </td>
			</tr>

			<tr>
				<td>Security Question</td>
				<td>
					<select name='question'>
						<?php $user->get_questions($db_question);?>
					</select>
				</td>
			</tr>

			<tr>
				<td>Security Answer</td>
				<td> <input type='text' name='answer' value='<?php echo $db_answer;?>' required /> </td>
			</tr>

			<tr>
				<td>
					<?php
						require 'includes/php/signup.functions.php';

						if(isset($_POST['profile_updated'])) {

							$firstname = check_submitted_firstname($_POST['firstname']);
							$lastname = check_submitted_lastname($_POST['lastname']);
							$gender = check_submitted_gender($_POST['gender']);
							$email = check_updated_email($_POST['email']);
							$question = check_submitted_question($_POST['question']);
							$answer = check_submitted_answer($_POST['answer']);

							if($firstname && $lastname) {
								if($gender) {
									if($email) {
										if($question && $answer) {

											$firstname 	= ucfirst(strtolower($_POST['firstname']));
											$lastname 	= ucfirst(strtolower($_POST['lastname']));
											$gender 	= $_POST['gender'];
											$email 		= strtolower($_POST['email']);
											$question 	= strtolower(strip_tags($_POST['question']));
											$answer 	= strtolower(strip_tags($_POST['answer']));

											$update_sql = "
												UPDATE `users` `u` INNER JOIN `verification` `v` USING(`user_id`)
												SET `u`.`firstname` = ?, `u`.`lastname` = ?, `u`.`gender` = ?, `u`.`email` = ?, `v`.`question` = ?, `v`.`answer` = ?
												WHERE `u`.`user_id` = {$_SESSION['user_id']}
											";

											if($update_stmt = $dbc->prepare($update_sql)) {
												$update_stmt->bind_param('ssssss', $firstname, $lastname, $gender, $email, $question, $answer);
												$update_stmt->execute();
											}

											if($update_stmt->affected_rows) {

												session_start();
												$_SESSION['firstname'] = $firstname;
												$_SESSION['lastname'] = $lastname;
												$_SESSION['gender'] = $gender;
												$_SESSION['email'] = $email;

												echo "<p class='success'>Profile successfully updated.</p>";
												header('Refresh:5;url=/profile');
												
											}else { echo "<p class='error'>Nothing has been changed.</p>"; }

											

										}else { echo "<p class='error'>Something is wrong with security question or answer.</p>"; }
									}else { echo "<p class='error'>Email is invalid.</p>"; }
								}else { echo "<p class='error'>Gender is fabricated.</p>"; }
							}else { echo "<p class='error'>Something is wrong with your name.</p>"; }
						}
					?>
				</td>
			</tr>

			<tr>
				<input type='hidden' name='profile_updated' value='true' />
				<td colspan='2' style='text-align: center;'> <input type='submit' value='Update Information' /> </td>
			</tr>
		</table>
	</form>
</div>