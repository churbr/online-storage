<script src='includes/js/friendreq.js'></script>

<h4>Friend Requests</h4>

<hr />	

<div id='profile_friendrequest_div'>

<?php

	$sql_friendreqs = '
		SELECT u.user_id, picture, firstname, lastname
		FROM friends f, users u
		WHERE f.userid_y = ' . $_SESSION['user_id'] . ' AND f.friendship_status = 0 AND f.userid_x = u.user_id
	';

	$query_friendreqs = $dbc->query($sql_friendreqs);

	if($query_friendreqs->num_rows) {

		while($requests = $query_friendreqs->fetch_object()) {
			$user_id = $requests->user_id;
			$picture = $requests->picture;
			$firstname = ucfirst($requests->firstname);
			$lastname = ucfirst($requests->lastname);
			$fullname = $firstname . ' ' . $lastname;

			$name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $fullname);
			$name_link = preg_replace('/[\s-]+/', ' ', $name_link);
			$name_link = preg_replace('/[\s_]/', '-', $name_link);
			$name_link = strtolower($name_link);


			echo <<<THE_REQUESTS

				<div id='thumbnail_container_$user_id'>
					<div class='profile_req_thumbnail'>
						<a href='images/users/$picture' data-lightbox='$firstname' data-title='$firstname'>
							<img src='images/users/$picture' />
						</a>

						<div class='form_name'>
							<a href='user/$user_id/$name_link'>
								<p>$firstname $lastname</p>
							</a>

							<p class='friendreq_action'>
								<button id='confirm_friendreq' class='confirm_button_$user_id' onclick='confirm_friendreq($user_id)'>Confirm request</button>
								<button id='delete_friendreq' class='delete_button_$user_id' onclick='delete_friendreq($user_id)'>Delete request</button>
							</p>
						</div>
						<div id='clear'></div>
					</div>
				</div>
THE_REQUESTS;
		}

		echo "<div id='clear'></div>";

	}else {
		echo '<div class="correct">You do not have friends yet.</div>';
	}

?>

</div>