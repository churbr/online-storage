<?php
	$browser = get_browser(null, true);
	$check_IE = intval(substr($browser['parent'], 3));

	if(in_array($check_IE, array(6, 7, 8))) { die('Please upgrade your browser to use this service!'); }
?>

<div id='upload_page_div'>
	<h1>Add new files</h1>
	<hr />
	<form id='file_upload' enctype='multipart/form-data'>
		<input type='file' id='the_file' name='file[]' multiple='multiple' />
		<input type='submit' id='upload_button' value='Upload' />
		<div id='clear'></div>
	</form>

	<div id='upload_progressbar'></div>
	<div id='percent_info'></div>
	<div id='view_uploaded_files'> <ul></ul> </div>
	<div id='file_upload_message'></div>
	
	<script src='<?php echo BASE_URL;?>includes/js/ajax_upload.js'></script>
</div>