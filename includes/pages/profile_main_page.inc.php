<div id='profile_main_page'>

<?php
	if($user->is_admin()) {

		echo '<h2>Welcome Admin!</h2>';

		echo <<<ADMIN_CONTROL
			<ul id='admin_control'>
				<li> <a href='admin/view_suggestions'>View Suggestions</a> </li>
				<li> <a href='admin/user_management'>User Management</a> </li>
				<li> <a href='admin/file_management'>File Management</a> </li>
			</ul>
ADMIN_CONTROL;

	}else {
?>

	<h2>My Files</h2>

	<table>
		<tr>
			<td> <a href='/docs'> <img src='images/resource/doc.png' /> Documents </a> </td>
			<td> <a href='/image'> <img src='images/resource/image.png' /> Images </a> </td>
			<td> <a href='/music'> <img src='images/resource/music.png' /> Music </a> </td>
			<td> <a href='/video'> <img src='images/resource/video.png' /> Video </a> </td>
			<td> <a href='/uncategorized'> <img src='images/resource/nocategory.png' /> Uncategorized </a> </td>
		</tr>

		<tr>
			<td> <a href='/apps'> <img src='images/resource/app.png' /> Applications </a> </td>
			<td> <a href='/archives'> <img src='images/resource/archive.png' /> Archives </a> </td>
		</tr>
	</table>

<?php
	}
?>

</div>