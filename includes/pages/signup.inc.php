<script src='<?php echo BASE_URL . 'includes/js/signup_validation.js';?>'></script>

<div id='signup'>

	<h4>Sign Up</h4>

	<div id='registration_info'></div>

	<div id='clear'></div>

	<hr />

	<form action='signup_validation' method='POST' id='signup_form'>
		<table>
			<tr>
				<td>Fullname</td>
				<td colspan='2'>
					<input type='text' name='firstname' id='fname_field' onkeyup='signup_form(fname_field)' placeholder='firstname' required />
					<input type='text' name='lastname' id='lname_field' onkeyup='signup_form(lname_field)' placeholder='lastname' required />
				</td>
			</tr>

			<tr>
				<td></td>
				<td>
					<div style='width: 420px;'>
						<p>
							<span id='fname_span' style='text-align: left;'></span>
							<span id='lname_span' style='text-align: right;'></span>
						</p>
					</div>
				</td>
			</tr>

			<tr>
				<td>Username</td>
				<td> <input type='text' name='username' id='username_field' onkeyup='signup_form(username_field)' required /> </td>
			</tr>

			<tr id='username_info'>
			</tr>

			<tr>
				<td>Password</td>
				<td> <input type='password' name='password' id='password_field' onkeyup='signup_form(password_field)' required /> </td>
			</tr>

			<tr id='password_info'>
			</tr>
			
			<tr>
				<td>Confirm password</td>
				<td> <input type='password' name='repassword' id='repassword_field' onkeyup='signup_form(repassword_field)' required /> </td>
			</tr>

			<tr id='repassword_info'>
			</tr>

			<tr>
				<td>Gender</td>
				<td id='radio_td'>
					<p>
						<span> <input type='radio' name='gender' id='gender_field' onclick='signup_form(gender_field)' value='male' checked /> Male </span>
						<span> <input type='radio' name='gender' id='gender_field' onclick='signup_form(gender_field)' value='female' /> Female </span>
					</p> 
				</td>
			</tr>

			<tr id='gender_info'>
			</tr>

			<tr>
				<td>Email</td>
				<td> <input type='text' name='email' size='35' id='email_field' onkeyup='signup_form(email_field)' required /> </td>
			</tr>

			<tr id='email_info'>
			</tr>

			<tr>
				<td>Security Question</td>
				<td>
					<select name='question' id='question_field' onclick='signup_form(question_field)'>
						<option>What is your pet's name?</option>
						<option>What's your favourite number?</option>
						<option>Who is your first crush?</option>
						<option>What is your favorite T.V. show?</option>
						<option>What do you usually have for breakfast?</option>
						<option>What foods do you dislike?</option>
						<option>What is the best book you've read?</option>
						<option>What is your all-time favorite movie?</option>
						<option>If your life were a song, what would the title be?</option>
					</select>
				</td>
			</tr>

			<tr id='secretquestion_info'>
			</tr>

			<tr>
				<td>Secret Answer</td>
				<td> <input type='text' name='answer' id='answer_field' onkeyup='signup_form(answer_field)' required /> </td>
			</tr>

			<tr id='secretanswer_info'>
			</tr>

			<tr>
				<td colspan='2' align='center'>
					<input type='submit' name='signedup' value='Sign Up!' />
				</td>
			</tr>
		</table>
	</form>
</div>