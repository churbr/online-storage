<?php
	require BASE_URI . 'includes/classes/admin.class.inc';
	$admin_user = new Admin;
?>

<div id='file_management_div'>
	<h2>File Management</h2>

	<hr />

<?php

	$sql_view_files = '
		SELECT f.file_id, file_name, file_bin, file_size, file_category, file_type, date_uploaded, u.firstname, lastname
		FROM files f, users u
		WHERE f.user_id = u.user_id
	';

	if(isset($_POST['action_edit'])) {

		$file_id 	  = intval($_POST['file_id']);
		$new_filename = htmlentities(strip_tags($_POST['filename']));
		$sharing_type = intval($_POST['privacy']);

		$dbc->query("UPDATE `files` SET `file_name` = '" . $dbc->escape_string($new_filename) . "', `file_accessibility` = " . $dbc->escape_string($sharing_type) . " WHERE `file_id` = " . $dbc->escape_string($file_id));
		
		if($dbc->affected_rows) {
			echo "<p class='success'>File changed successfully.</p>";
		}else { echo "<p class='error'>Nothing has been changed.</p>"; }
	}


	if(isset($_POST['admin_action'])) {

		$action = htmlentities(strip_tags($_POST['admin_action']));
		$file_id = htmlentities(strip_tags(intval($_POST['file_id'])));

		switch ($action) {
			case 'edit_file':

				$file_id = intval($_POST['file_id']);

				$sql_get_filedata = 'SELECT * FROM `files` WHERE `file_id` = ' . $file_id;

				$query_filedata = $dbc->query($sql_get_filedata);

				$result_file_data = $query_filedata->fetch_object();
				$file_name = $result_file_data->file_name;
				$fileshare = intval($result_file_data->file_accessibility);

?>
					<div style='border: 1px solid blue; padding: 5px;'>
						<h3>Update File</h3>
						<form action='' method='POST'>
							<p>Filename: <input type='text' name='filename' value='<?php echo $file_name; ?>'/></p>
							<p> Shared to:
								<select name='privacy'>
									<option <?php echo ($fileshare === 1) ? 'selected':''; ?> value='1'>Public</option>
									<option <?php echo ($fileshare === 0) ? 'selected':''; ?> value='0'>Private</option>
								</select>
							</p>
							<input type='hidden' name='file_id' value='<?php echo $file_id;?>' />
							<input type='hidden' name='action_edit' value='true' />
							<input type='submit' value='Update file' />
						</form>
					</div>
<?php
			break;
			
			case 'delete_file':
				$file_id = intval($_POST['file_id']);

				$dbc->query("DELETE FROM `files` WHERE `file_id` = " . $file_id);

				if($dbc->affected_rows) {
					echo "<p class='success'>File deleted successfully.</p>";
				}else {
					echo "<p class='success'>Unable to delete file.</p>";
				}

			break;

			default:
				echo "<script>alert('Something went wrong.');</script>";
			break;
		}
	}

	$query_view_files = $dbc->query($sql_view_files);

	if($query_view_files->num_rows) {
		echo '<div id="admin_view_filelist">';

		while($fileinfo = $query_view_files->fetch_object()) {
			$file_id = $fileinfo->file_id;
			$firstname = $fileinfo->firstname;
			$lastname = $fileinfo->lastname;
			$file_name = $fileinfo->file_name;
			$file_bin = $fileinfo->file_bin;
			$file_size = $fileinfo->file_size;
			$file_category = $fileinfo->file_category;
			$file_type = $fileinfo->file_type;
			$date_uploaded = $fileinfo->date_uploaded;
			$file_icon = $download->file_icon($file_category);

?>

			<div class='each_file'>
				<img src='<?php echo BASE_URL . 'images/resource/' . $file_icon;?>' />
				<p>
					<?php
						echo "
							<strong>FILENAME</strong>: $file_name <br />
							<strong>Size</strong>: $file_size <br />
							<strong>Type</strong>: $file_type <br />
							<strong>Uploader</strong>: $firstname $lastname  <br />
							<strong>Date Uploaded</strong>: $date_uploaded <br />
						";
					?>
				</p>

				<form action='' method='POST'>
					<input type='hidden' name='file_id' value='<?php echo $file_id;?>' />
					<select name='admin_action'>
						<option value='edit_file'>Edit</option>
						<option value='delete_file'>Delete</option>
					</select>
					<input type='submit' value='Go' />
				</form>

				<div id='clear'></div>
			</div>

<?php
		}

		echo '</div>';

	}else {
		echo "<p>Users have not uploaded any files yet.</p>";
	}
?>
</div>