<?php
	if(isset($_GET['user_view_file'])) {
?>

<div id='profile_music_div'>
	<h1> <img src='<?php echo BASE_URL . 'images/resource/music.png'?>' /> <p> Music Files </p> </h1>
	
	<div id='clear'></div> <hr />

	<?php

		$sql_query_music = "SELECT * FROM `files` WHERE `file_category` = 'music' AND `user_id` = $info_user_id";

		$query_music_file = $dbc->query($sql_query_music);

		$icon_path = BASE_URL . 'images/resource/download.png';

		if($query_music_file->num_rows) {

			echo '<table id="music_table_view"> <tr>';

			while($music_file_data = $query_music_file->fetch_object()) {
				$file_id = $music_file_data->file_id;
				$file_name = $music_file_data->file_name;
				$file_bin = $music_file_data->file_bin;
				$file_bin_ogg = explode('.', $file_bin);
				$file_bin_ogg = $file_bin_ogg[0];
				$file_size = $music_file_data->file_size;
				$date_uploaded = $music_file_data->date_uploaded;
				$file_accessibility = $music_file_data->file_accessibility;
				$file_share_type = (intval($file_accessibility) === 1) ? 'Public' : 'Private';

				if(intval($file_accessibility) === 1) {

?>
					<td>
						<p style='margin: 3px 0;'> <span class='filename' style='font-weight: bold;'><?php echo $file_name; ?></span> </p>

						<audio controls>
							<source src='<?php echo BASE_URL . 'files/music/' . $file_bin_ogg . '.ogg'; ?>' type='audio/ogg' />
							<source src='<?php echo BASE_URL . 'files/music/' . $file_bin; ?>' />
						</audio>

						<div id='clear'></div>

						<div class='video_form'>
							<form action='' method='POST'>
								<input type='hidden' name='file_id' value='<?php echo $file_id; ?>' />
								<input type='hidden' name='downloaded' value='true' />
								<input class='download_button' type='submit' value='Download (<?php echo $file_size; ?>)' />
							</form>
						</div>

						<div id='clear'></div>
					</td>
<?php
				}
			}

			echo '</tr> </table>';

		}else { echo "<p class='error'>User haven't uploaded music files yet.</p>"; }
	?>

	<div id='clear'></div>
</div>

<?php
	}else {
?>

<div id='profile_music_div'>
	<h1> <img src='<?php echo BASE_URL . 'images/resource/music.png'?>' /> <p> Music Files </p> </h1>
	
	<div id='clear'></div> <hr />

	<?php
		if($edit_file_info) {
			if(is_array($download->get_filedata($_POST['file_id']))) {
					$file = $download->get_filedata($_POST['file_id']);

					$filename = $file['file_name'];
					$filedesc = $file['file_desc'];
					$fileshare = intval($file['file_accessibility']);
					$filecategory = $file['file_category'];
?>

			<div id='edit_music_fileinfo'>
				<form id='edit_file_info'>
					<div class='filename'>
						<p class='title'>Filename</p>
						<input type='text' id='filename' value='<?php echo $filename; ?>' />
					</div>

					<div class='filedesc'>
						<p class='title'>Description</p>
						<textarea id='filedesc' placeholder='No description...'><?php echo $filedesc;?></textarea>
					</div>

					<div class='sharetype'>
						<p class='title'>Shared to</p>
						<select id='sharedto'>
							<option <?php echo ($fileshare === 1) ? 'selected':''; ?> value='1'>Public</option>
							<option <?php echo ($fileshare === 0) ? 'selected':''; ?> value='0'>Only me</option>
						</select>
					</div>

					<div class='category'>
						<p class='title'>Move to</p>
						<select id='moveto'>
							<?php
								$file_category_list = array('music', 'video', 'application', 'document', 'archive', 'image', 'nocategory');

								foreach ($file_category_list as $category) {
									if($filecategory == $category) {
										echo "<option selected>$category</option>";
									}else {
										echo "<option>$category</option>";
									}
								}
							?>
						</select>
					</div>

					<div class='button'>
						<input type='hidden' id='id' value='<?php echo $_POST['file_id']; ?>' />
						<input type='submit' value='Done' />
					</div>
					<div id='clear'></div>
				</form>


				<div id='edit_file_info_msg'></div>

				<a href='#' class='close'>Close</a>

				<div id='clear'></div>
			</div>
	
<?php
			}else { echo "<p class='error'>File entry doesn't exist.</p>"; }
		}

	
		if(!empty($delete_status)) { echo $delete_status; }

		$sql_query_music = "SELECT * FROM `files` WHERE `file_category` = 'music' AND `user_id` = {$_SESSION['user_id']}";

		$query_music_file = $dbc->query($sql_query_music);

		if($query_music_file->num_rows) {

			echo '<table id="music_table_view"> <tr>';

			while($music_file_data = $query_music_file->fetch_object()) {
				$file_id = $music_file_data->file_id;
				$file_name = $music_file_data->file_name;
				$file_bin = $music_file_data->file_bin;
				$file_bin_ogg = explode('.', $file_bin);
				$file_bin_ogg = $file_bin_ogg[0];
				$file_size = $music_file_data->file_size;
				$date_uploaded = $music_file_data->date_uploaded;
				$file_accessibility = $music_file_data->file_accessibility;
				$file_share_type = (intval($file_accessibility) === 1) ? 'Public' : 'Private';
				
?>
				<td>
					<p style='margin: 3px 0;'> <span class='filename' style='font-weight: bold;'><?php echo $file_name; ?></span> </p>

					<audio controls>
						<source src='<?php echo BASE_URL . 'files/music/' . $file_bin_ogg . '.ogg'; ?>' type='audio/ogg' />
						<source src='<?php echo BASE_URL . 'files/music/' . $file_bin; ?>' />
					</audio>

					<p style='margin: 3px 0;'>
						<span>Shared to: </span> <span class='share'><?php echo $file_share_type; ?></span> <span class='size'><?php echo $file_size; ?></span> <br />
					</p>

					<div id='clear'></div>

					<div class='audio_form'>
						<form action='' method='POST'>
							<input type='hidden' name='file_id' value='<?php echo $file_id; ?>' />
							<select name='user_action'>
								<option value='download'>Download</option>
								<option value='view'>View</option>
								<option value='edit'>Edit</option>
								<option value='delete'>Delete</option>
							</select>
							<input class='go_button' type='submit' name='action_performed' value='GO' />
						</form>
					</div>

					<div id='clear'></div>
				</td>
<?php
			}

			echo '</tr> </table>';

		}else { echo "<p class='error'>You have no music files uploaded yet.</p>"; }
	?>

	<div id='clear'></div>
</div>

<?php
	}
?>