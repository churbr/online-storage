<?php
	if(!$user->is_admin()) {
		header('Location: /profile');
	}

	if(isset($_POST['delete_sugg'])) {
		$suggestion_id = htmlentities(intval($_POST['suggestion_id']));

		$sql_delete_sugg = 'DELETE FROM `suggestions` WHERE id = ' . $suggestion_id;

		$query_delete_sugg = $dbc->query($sql_delete_sugg);

		if(!$dbc->affected_rows) {
			echo 'Something went wrong.';
		}
	}
?>

<div id='admin_suggestions_div'>
	<h2>User Suggestions</h2>

	<hr />

	<?php
		$sql_view_suggestions = '
			SELECT s.id, fullname, email, message, date_received, f.file_name
			FROM suggestions s, files f
			WHERE s.file_id = f.file_id
		';

		$query_view_suggestions = $dbc->query($sql_view_suggestions);

		if($query_view_suggestions->num_rows) {

			while($sugg = $query_view_suggestions->fetch_object()) {
				$id = $sugg->id;
				$from = $sugg->fullname;
				$email = $sugg->email;
				$message = $sugg->message;
				$date = $sugg->date_received;
				$source = $sugg->file_name;

				echo <<<SUGGESTIONS
				<p>
					<strong>FROM</strong>: $from (<a href='#'>$email</a>)<br /> <br />
					<strong>MESSAGE</strong>: <br /> <span  style='font-style: italic;'>$message </span> <br /> <br />
					<strong>DATE RECEIVED</strong>: $date <br />
					<strong>SOURCE</strong>: $source

					<form action='' method='POST'>
						<input type='hidden' name='delete_sugg' value='true' />
						<input type='hidden' name='suggestion_id' value='$id' />
						<input type='submit' value='Delete' />
					</form>
				</p> 
SUGGESTIONS;
			}
		}else {
			echo '<p class="correct">No suggestions received from user yet.</p>';
		}
	?>

	<div id='clear'></div>
</div>