<?php

	class hydra_file_framework {

		private static $document_extensions = array('pdf', 'doc', 'dot', 'docx', 'docm', 'dotx', 'dotm', 'docb', 'xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', 'ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm', 'txt', 'sql', 'csv', 'srt', 'html', 'sub', 'conf', 'htm', 'xml', 'rtf', 'asp', 'tt', 'manifest', 'log', 'stf', 'ascii', 'rtx', 'dat', 'ttxt', 'xhtm', 'inc');

		private static $music_extensions = array('mp3', 'cda', 'wpl', 'gp5', 'amr', 'cdfs', 'ram', 'wav', 'm4a', 'caf', 'wma', 'asx', 'aac', 'spx', 'ogg', 'au', 'm4p', 'm4r', 'dlp', 'efa', 'band', 'mp4b', 'asf', 'shn', 'oga', 'stmp', 'm4b', 'mp4a', 'ra', 'mka', 'ima4', 'cdda', 'rma', 'ac3', 'nsa', 'rmj', 'raw', 'wm', 'mpga', 'mpa');

		private static $video_extensions = array('mp4', 'avi', 'mkv', 'mov', '264', 'h264', 'webm', 'flv', 'swf', 'vob', 'ogv', '3gp', 'wmv', 'mpg', 'wma', 'asx', 'm4v', 'mpg4', '3gpp', 'movie', 'mpcpl', '3gp2', 'divx', 'moo', 'moov', 'wm', 'ogm', 'qt', 'hdmov');

		private static $application_extensions = array('exe', 'msi', 'iso');

		private static $archive_extensions = array('rar', 'rar5', 'zip', 'cbr', 'cbz');

		private static $image_extensions = array('gif', 'jpeg', 'jpg', 'pjpeg', 'x-png', 'png');



		function file_category($file_extension) {


			$file_category = NULL;

			if(!empty($file_extension)) {

				if(in_array($file_extension, self::$document_extensions)) {
					$file_category = 'document';
				}elseif(in_array($file_extension, self::$music_extensions)) {
					$file_category = 'music';
				}elseif(in_array($file_extension, self::$video_extensions)) {
					$file_category = 'video';
				}elseif(in_array($file_extension, self::$application_extensions)) {
					$file_category = 'application';
				}elseif(in_array($file_extension, self::$archive_extensions)) {
					$file_category = 'archive';
				}elseif(in_array($file_extension, self::$image_extensions)) {
					$file_category = 'image';
				}else{
					$file_category = 'nocategory';
				}
			}

			return $file_category;
		}

		function file_icon($file_type) {
			$icon = NULL;

			switch ($file_type) {
				case 'document':
					$icon = 'doc.png';
				break;

				case 'application':
					$icon = 'app.png';
				break;

				case 'music':
					$icon = 'music.png';
				break;

				case 'video':
					$icon = 'video.png';
				break;

				case 'archive':
					$icon = 'archive.png';
				break;

				case 'image':
					$icon = 'image.png';
				break;

				default:
					$icon = 'nocategory.png';
				break;
			}

			return $icon;
			
		}

		private function select_folder($file_category) {
			$folder_name = 'nocategory';

			switch ($file_category) {
				case 'application':
					$folder_name = 'app';
				break;

				case 'archive':
					$folder_name = 'archive';
				break;

				case 'document':
					$folder_name = 'doc';
				break;

				case 'image':
					$folder_name = 'image';
				break;

				case 'music':
					$folder_name = 'music';
				break;

				case 'video':
					$folder_name = 'video';
				break;

				default:
					$folder_name = 'nocategory';
				break;
			}
			return $folder_name;
		}

		function file_download($file_id) {

			require 'includes/php/db.inc';

			$file_data = NULL;

			$sql = '
				SELECT `file_name`, `file_bin`, `file_category`, `file_type`
				FROM `files` WHERE `file_id` = 
			' . $file_id;

			$dbc->query('UPDATE `files` SET `file_downloaded` = `file_downloaded`+1 WHERE `file_id` = ' . $file_id);
			
			$query = $dbc->query($sql);

			if($query->num_rows) {
				$result = $query->fetch_object();

				$name 		= $result->file_name;
				$bin 		= $result->file_bin;
				$category 	= $result->file_category;
				$type 		= $result->file_type;
				$folder 	= self::select_folder($category);

				$file_data = array(
					'file_name' => $name,
					'file_bin' => $bin,
					'file_type' => strtolower($type),
					'file_folder' => $folder
				);

				$dbc->close();

			}
			return $file_data;
		}

		function FILE($file_id) {

			require 'includes/php/config.inc';

			$file_data = self::file_download($file_id);

			if(is_array($file_data)) {
				$file_name = $file_data['file_name'] . '.' . $file_data['file_type'];
				$file_bin = $file_data['file_bin'];
				$file_folder = $file_data['file_folder'];

				header('Content-type: text/plain');
				header('Content-Disposition: attachment; filename=' . $file_name);
				header('Content-Length: ' . filesize(BASE_URI . 'files/' . $file_folder . '/' . $file_bin));
				readfile(BASE_URI . 'files/' . $file_folder . '/' . $file_bin);
			}
		}

		function view_file ($file_id) {

			require 'includes/php/config.inc';
			require DB;

			$sql_view_file = 'SELECT `file_name` FROM `files` WHERE `file_id` = ' . $file_id;

			$query_filename = $dbc->query($sql_view_file);

			$file_data = $query_filename->fetch_object();

			$file_name = $file_data->file_name;

			$name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $file_name);
			$name_link = preg_replace('/[\s-]+/', ' ', $name_link);
			$name_link = preg_replace('/[\s_]/', '-', $name_link);

			$dbc->close();

			header('Location: ' . BASE_URL . "view/$file_id/$name_link");
		}

		function get_filedata($file_id) {

			require 'includes/php/db.inc';

			$filedata_array = array();

			$file_id = htmlentities(strip_tags(intval($file_id)));

			$sql_filedata = 'SELECT `file_name`, `file_bin`, `file_desc`, `file_accessibility`, `file_category` FROM `files` WHERE `file_id` = ' . $file_id . ' AND `user_id` = ' . $_SESSION['user_id'];

			$query_filedata = $dbc->query($sql_filedata);

			if($query_filedata->num_rows) {

				$filedata = $query_filedata->fetch_object();

				$filedata_array['file_name'] = $filedata->file_name;
				$filedata_array['file_bin'] = $filedata->file_bin;
				$filedata_array['file_desc'] = $filedata->file_desc;
				$filedata_array['file_accessibility'] = $filedata->file_accessibility;
				$filedata_array['file_category'] = $filedata->file_category;

				return $filedata_array;
			}else {
				return false;
			}
		}

		function delete_file($file_id) {

			require 'includes/php/db.inc';

			$delete_status = null;

			$file_id = intval($file_id);
			$file = self::get_filedata($file_id);

			$file_bin = $file['file_bin'];
			$file_dir = self::select_folder($file['file_category']);
			$file_path = 'files/' . $file_dir . '/' . $file_bin;

			$sql_del_file = "DELETE FROM `files` WHERE `file_id` = $file_id AND `user_id` = {$_SESSION['user_id']}";

			if(file_exists($file_path)) {
				$del_file_query = $dbc->query($sql_del_file);

				if(!$dbc->affected_rows || !unlink($file_path)) {
					$delete_status .= "<p class='error'>You can't delete other people's files.</p>";
				}
			} else {
				$delete_status .= "<p class='error'>File doesn't exist.</p>";
			}

			return $delete_status;
		}
	}

?>