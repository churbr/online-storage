<?php
	class file_upload {

		public 	function make_random_filename($file_name) {

			$characters = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5,6,7,8,9,0';
			$characters_array = explode(',', $characters);
			shuffle($characters_array);
			$randomed_filename = implode('', $characters_array);

			$file_extension = explode('.', $file_name);
			$file_extension = end($file_extension);
			$file_extension = strip_tags(htmlentities($file_extension));

			return ($randomed_filename.'.'.$file_extension);
		}

		public function get_filename($filename) {
			$filename = explode('.', $filename);
			return $filename[0];
		}

		public function validate_uploaded_file($file) {

			$name 		= $file['file']['name'][0];
			$type 		= $file['file']['type'][0];
			$tmp_name 	= $file['file']['tmp_name'][0];
			$error 		= $file['file']['error'][0];
			$size 		= $file['file']['size'][0];

			if(!empty($name) && !empty($type) && !empty($tmp_name) && $size > 0 && $error == 0) {
				return TRUE;
			}else{
				return FALSE;
			}
		}

		public function format_file_size($bytes) {
	        if($bytes >= 1073741824) {
	            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
	        }elseif ($bytes >= 1048576) {
	            $bytes = number_format($bytes / 1048576, 2) . ' MB';
	        }elseif ($bytes >= 1024) {
	            $bytes = number_format($bytes / 1024, 2) . ' KB';
	        }elseif ($bytes > 1) {
	            $bytes = $bytes . ' bytes';
	        }elseif ($bytes == 1) {
	            $bytes = $bytes . ' byte';
	        }else {
	            $bytes = '0 bytes';
	        }return $bytes;
		}

		public function file_size_level($formatted_file_size) {

			$filesize_level = explode(' ', $formatted_file_size);
			return $filesize_level[1];
		}

		public function select_folder($file_category) {
			$folder_name = 'nocategory';

			switch ($file_category) {
				case 'application':
					$folder_name = 'app';
				break;

				case 'archive':
					$folder_name = 'archive';
				break;

				case 'document':
					$folder_name = 'doc';
				break;

				case 'image':
					$folder_name = 'image';
				break;

				case 'music':
					$folder_name = 'music';
				break;

				case 'video':
					$folder_name = 'video';
				break;

				default:
					$folder_name = 'nocategory';
				break;
			}
			return $folder_name;
		}

		public function select_link($file_category) {
			$folder_name = 'uncategorized';

			switch ($file_category) {
				case 'application':
					$folder_name = 'apps';
				break;

				case 'archive':
					$folder_name = 'archives';
				break;

				case 'document':
					$folder_name = 'docs';
				break;

				case 'image':
					$folder_name = 'image';
				break;

				case 'music':
					$folder_name = 'music';
				break;

				case 'video':
					$folder_name = 'video';
				break;

				default:
					$folder_name = 'uncategorized';
				break;
			}
			return $folder_name;
		}

		function check_filename($filename) {
			return preg_match('#^[a-zA-Z0-9 _\-\(\)\'\"\.]{3,50}$#', trim($filename));
		}

		function check_fileshare($fileshare) {
			if(in_array($fileshare, array(1, 0))) {
				return true;
			}else {
				return false;
			}
		}

		function check_filecategory($filecategory) {
			if(in_array($filecategory, array('music', 'video', 'application', 'document', 'archive', 'image', 'nocategory'))) {
				return true;
			}else {
				return false;
			}
		}		
	}
?>