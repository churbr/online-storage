<?php

	require $_SERVER['DOCUMENT_ROOT'] . '/includes/php/db.inc';
	error_reporting(0);

	class Admin {

		private function select_folder($file_category) {
			$folder_name = 'nocategory';

			switch ($file_category) {
				case 'application':
					$folder_name = 'app';
				break;

				case 'archive':
					$folder_name = 'archive';
				break;

				case 'document':
					$folder_name = 'doc';
				break;

				case 'image':
					$folder_name = 'image';
				break;

				case 'music':
					$folder_name = 'music';
				break;

				case 'video':
					$folder_name = 'video';
				break;

				default:
					$folder_name = 'nocategory';
				break;
			}
			return $folder_name;
		}


		function admin_delete_user($user_id) {

			global $dbc;

			$user_id = intval($user_id);

			$sql_filedata 		= 'SELECT `file_bin`, `file_category` FROM `files` WHERE `user_id` = ' . $user_id;
			$sql_deluser 		= 'DELETE FROM `users` WHERE `user_id` = ' . $user_id;

			$query_filedata = $dbc->query($sql_filedata);

			if($query_filedata->num_rows) {
				while($filedata = $query_filedata->fetch_object()) {

					$file_bin 		= $filedata->file_bin;
					$file_category 	= $filedata->file_category;
					$file_dir 		= self::select_folder($file_category);
					$file_path 		= 'files/' . $file_dir . '/' . $file_bin;

					unlink($file_path);
				}
			}

			$delete_user 	= $dbc->query($sql_deluser);

			if($dbc->affected_rows) {
				echo '<p class="success">User successfully deleted.</p>';
			}
		}

		function admin_user_block($user_id, $reason) {

			global $dbc;
			$user_id = intval($user_id);

			$sql_check_instance = 'SELECT * FROM `blocklist` WHERE `blocked_userid` = ' . $user_id;
			$sql_insert_new_blocklist_item = 'INSERT INTO `blocklist` (`blocklist_no`, `blocked_userid`, `reason`, `date_blocked`) VALUES(NULL, ' . $user_id . ', "' . $dbc->escape_string($reason) . '", NOW())';

			$query_check_instance = $dbc->query($sql_check_instance);

			if($query_check_instance->num_rows) {
				echo "<p class='error'>User already has a record in blocklist.</p>";
			}else {
				$insert_new_blocklist_item = $dbc->query($sql_insert_new_blocklist_item);

				if($dbc->affected_rows) {
					echo "<p class='correct'>User successfully blocked.</p>";
				}
			}
		}

		function admin_user_search($keyword) {
			global $dbc;

			$sql_search = '
				SELECT `user_id`, `username`, `picture`, `firstname`, `lastname`, `gender`, `email`, `user_type`
				FROM `users`
				WHERE `user_id` != ' . $_SESSION['user_id'] . ' AND `firstname` LIKE "%' . $dbc->escape_string($keyword) . '%" OR
				`lastname` LIKE "%' . $dbc->escape_string($keyword) . '%"
			';

			$query_search = $dbc->query($sql_search);

			if($query_search->num_rows) {

				echo '<div id="admin_view_userlist">';

				while($search_result = $query_search->fetch_object()) {
					$user_id 	= $search_result->user_id;
					$username 	= $search_result->username;
					$picture 	= $search_result->picture;
					$firstname 	= $search_result->firstname;
					$lastname 	= $search_result->lastname;
					$gender 	= $search_result->gender;
					$email 		= $search_result->email;
					$user_type 	= $search_result->user_type;

					if($user_id != $_SESSION['user_id']) {
?>

						<div class='each_user'>
							<img src='<?php echo BASE_URL . 'images/users/' . $picture;?>' />
							<p>
								<?php
									echo "
										$firstname $lastname ($username) <br />
										$gender <br />
										$user_type <br />
									";
								?>
							</p>

							<form action='' method='POST'>
								<input type='hidden' name='user_id' value='<?php echo $user_id;?>' />
								<select name='admin_action'>
									<option value='block_user'>Block</option>
									<option value='delete_user'>Delete</option>
								</select>
								<input type='submit' value='Go' />
							</form>

							<div id='clear'></div>
						</div>

<?php
					}
				}

				echo '</div>';

			}else {
				echo "<p class='error'>No result.</p>";
			}
		}

	}
?>