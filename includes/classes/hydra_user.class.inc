<?php
	require $_SERVER['DOCUMENT_ROOT'] . '/includes/php/db.inc';
	error_reporting(E_ALL & ~E_NOTICE);

	class User {

		function validate_name($name) {
			if(preg_match('#^[a-zA-Z-\s]{2,}$#', $name)==1) {
				return true;
			}else{
				return false;
			}
		}

		function validate_username($username) {
			return preg_match('#^[a-z0-9_-]{3,25}$#', $username);
		}

		function username_exists($username) {
			global $dbc;
			$username_existed = FALSE;

			$query = $dbc->query("SELECT `username` FROM `users` WHERE `username` = '$username'");

			if($query->num_rows) {
				$username_existed = TRUE;
			}

			return $username_existed;
		}

		function user_exists($user_id) {
			global $dbc;
			$user_existed = FALSE;

			$query = $dbc->query("SELECT `user_id` FROM `users` WHERE `user_id` = $user_id");

			if($query->num_rows) {
				$user_existed = TRUE;
			}

			return $user_existed;
		}

		function check_password($password) {
			if(strlen($password) >= 8 && strlen($password) <= 30) {
				return TRUE;
			}else{
				return FALSE;
			}
		}
				
		function validate_gender($gender) {
			$status = FALSE;

			if(in_array($gender, array('male', 'female'))) {
				$status = TRUE;
			}

			return $status;
		}

		function email_exists($email) {
			global $dbc;
			$email_exists = FALSE;

			$query = $dbc->query("SELECT `email` FROM `users` WHERE `email` = '$email'");

			if($query->num_rows) {
				$email_exists = TRUE;
			}

			return $email_exists;
		}


		function validate_email($email) {
			$status = FALSE;

			if(preg_match('#^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$#', $email)) {
				$status = TRUE;
			}

			return $status;
		}

		function validate_questions($question) {

			$status = FALSE;

			$builtin_questions = array(
				"What is your pet's name?",
				"What's your favourite number?",
				"Who is your first crush?",
				"What is your favorite T.V. show?",
				"What do you usually have for breakfast?",
				"What foods do you dislike?",
				"What is the best book you've read?",
				"What is your all-time favorite movie?",
				"If your life were a song, what would the title be?"
			);

			if(in_array($question, $builtin_questions)) {
				$status = TRUE;
			}

			return $status;
		}

		function get_questions($default_question) {

			$builtin_questions = array(
				"What is your pet's name?",
				"What's your favourite number?",
				"Who is your first crush?",
				"What is your favorite T.V. show?",
				"What do you usually have for breakfast?",
				"What foods do you dislike?",
				"What is the best book you've read?",
				"What is your all-time favorite movie?",
				"If your life were a song, what would the title be?"
			);

			foreach ($builtin_questions as $question) {
				if(strtolower($question) == strtolower($default_question)) {
					echo "<option selected>$question</option>";
				}else { echo "<option>$question</option>"; }
			}
		}

		function validate_answer($answer) {
			if(strlen($answer)>=3 && strlen($answer)<=100) {
				return TRUE;
			}else{
				return FALSE;
			}
		}

		function add_new_user($firstname, $lastname, $username, $password, $gender, $email, $question, $answer) {

			global $dbc;
			$success = FALSE;

			$firstname 	= ucfirst(strtolower($firstname));
			$lastname 	= ucfirst(strtolower($lastname));
			$username 	= strtolower($username);
			$password 	= md5($password);
			$email 		= strtolower($email);
			$answer 	= strtolower(strip_tags($answer));

			$users_statement = $dbc->prepare("INSERT INTO `users` (`user_id`, `username`, `password`, `picture`, `firstname`, `lastname`, `gender`, `email`, `user_type`) VALUES (NULL, ?, ?, 'default.png', ?, ?, ?, ?, 'member')");
			$users_statement->bind_param('ssssss', $username, $password, $firstname, $lastname, $gender, $email);
			$users_statement->execute();

			$verification_statement = $dbc->prepare("INSERT INTO `verification` (`id`, `user_id`, `question`, `answer`) VALUES(NULL, (SELECT `user_id` FROM `users` WHERE `username` = ?), ?, ?)");
			$verification_statement->bind_param('sss', $username, $question, $answer);
			$verification_statement->execute();

			$users = $users_statement->affected_rows;
			$verification = $verification_statement->affected_rows;

			if($users == 1 && $verification == 1) {
				$success = TRUE;
			}

			return $success;
		}

		function get_userid($username) {
			global $dbc;

			$query_userid = $dbc->query("SELECT `user_id` FROM `users` WHERE `username` = '$username'");
			
			if($query_userid->num_rows) {
				$data = $query_userid->fetch_object();
				$user_id = $data->user_id;
				return $user_id;
			}else {
				return FALSE;
			}
		}

		function user_blocked($user_id) {
			$status = FALSE;
			global $dbc;

			$query_blocklist = $dbc->query("SELECT `reason` FROM `blocklist` WHERE `blocked_userid` = $user_id");

			if($query_blocklist->num_rows) {
				$status = TRUE;
			}

			return $status;
		}

		function db_password_matched($username, $password) {
			global $dbc;

			$query_password = $dbc->query("SELECT `password` FROM `users` WHERE `username` = '$username'");
			$data = $query_password->fetch_object();

			$post_password = $dbc->escape_string($password);
			$db_password = $data->password;

			if(strcmp(md5($post_password), $db_password)===0) {
				return TRUE;
			}else {
				return FALSE;
			}
		}

		function establish_session($username) {
			global $dbc;

			$sql = "
				SELECT 	`user_id`,
						`username`,
						`picture`,
						`firstname`,
						`lastname`,
						`gender`,
						`email`,
						`user_type`
				FROM `users`
				WHERE `username` = '$username'
			";

			$query_userinfo = $dbc->query($sql);
			$data = $query_userinfo->fetch_object();

			session_start();
			$_SESSION['user_id'] = $data->user_id;
			$_SESSION['username'] = $data->username;
			$_SESSION['picture'] = $data->picture;
			$_SESSION['firstname'] = $data->firstname;
			$_SESSION['lastname'] = $data->lastname;
			$_SESSION['gender'] = $data->gender;
			$_SESSION['email'] = $data->email;
			$_SESSION['user_type'] = $data->user_type;
		}

		function is_loggedin() {
			session_start();
			$is_loggedin = FALSE;

			if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['picture'], $_SESSION['firstname'], $_SESSION['lastname'], $_SESSION['gender'], $_SESSION['email'], $_SESSION['user_type'])) {
				$is_loggedin = TRUE;
			}

			return $is_loggedin;
		}

		function is_admin() {
			session_start();

			if(strcmp($_SESSION['user_type'], 'admin') === 0) {
				return true;
			}else {
				return false;
			}
		}

		function check_image(&$image) {
			$file 		= $image['tmp_name'];
			$file_type	= $image['type'];
			$file_name	= $image['name'];
			$file_error = FALSE;

			if(!empty($file) && !empty($file_type) && !empty($file_name)) {

				$check_product_image_msg = '';

				if(strcmp('image', substr($file_type, 0, 5))===0){
					if(getimagesize($file)==TRUE){
						return TRUE;
					}
				}
			}

			return $file_error;
		}

		function friendship_status($profile_id) {

			session_start();
			global $dbc;

			$friendship_status = '';

			$sql_friendship_status = '
				SELECT `friendship_status`
				FROM `friends`
				WHERE `userid_x` = ' . $_SESSION['user_id'] . ' AND `userid_y` = ' . $profile_id . ' OR `userid_x` = ' . $profile_id . ' AND `userid_y` = ' . $_SESSION['user_id'];


			$query_friendship_status = $dbc->query($sql_friendship_status);

			if($query_friendship_status->num_rows) {
				$data = $query_friendship_status->fetch_object();

				$request_confirmation = $data->friendship_status;
				
				if($request_confirmation == 1) {
					$friendship_status = 'friends';
				}else {
					$friendship_status = 'not_confirmed';
				}

			}else {
				$friendship_status = 'no_record';
			}

			return $friendship_status;
		}

		function count_friends() {

			session_start();
			global $dbc;

			$sql_count_friends = 'SELECT COUNT(`id`) AS count FROM `friends` WHERE `userid_x` = ' . $_SESSION['user_id'] . ' AND `friendship_status` = 1 OR `userid_y` = ' . $_SESSION['user_id'] . ' AND `friendship_status` = 1';

			$count_friends_query = $dbc->query($sql_count_friends);

			if($count_friends_query->num_rows) {

				$count = $count_friends_query->fetch_object();

				echo $count->count;
			}else {
				echo '0';
			}
		}

		function count_friendreqs() {

			session_start();
			global $dbc;
			
			$sql_count_friendreqs = 'SELECT COUNT(`id`) AS count FROM `friends` WHERE `userid_y` = ' . $_SESSION['user_id'] . ' AND `friendship_status` = 0';

			$count_friendreqs_query = $dbc->query($sql_count_friendreqs);

			if($count_friendreqs_query->num_rows) {

				$count = $count_friendreqs_query->fetch_object();

				echo $count->count;
			}else {
				echo '0';
			}	
		}

		function send_request($profile_userid) {

			session_start();
			global $dbc;
			$profile_userid = htmlentities(stripcslashes(intval($profile_userid)));
			$friendship_status = self::friendship_status($profile_userid);

			$sql_check_requester = 'SELECT `userid_x`, `userid_y` FROM `friends` WHERE `userid_x` = ' . $_SESSION['user_id'] . ' AND `userid_y` = ' . $profile_userid . ' AND `friendship_status` = 0 OR `userid_x` = ' . $profile_userid . ' AND `userid_y` = ' . $_SESSION['user_id'] . ' AND `friendship_status` = 0';

			$check_requester_query = $dbc->query($sql_check_requester);

			$crq_result = $check_requester_query->fetch_object();

			$userid_x = intval($crq_result->userid_x);
			$userid_y = intval($crq_result->userid_y);

			if(strcmp($friendship_status, 'not_confirmed')===0) {
				if($userid_x === intval($_SESSION['user_id'])) {
					$sql_cancel_request = 'DELETE FROM `friends` WHERE `userid_x` = ' . $_SESSION['user_id'] . ' AND `userid_y` = ' . $profile_userid . ' AND `friendship_status` = 0';

					$cancel_request_query = $dbc->query($sql_cancel_request);

					if($dbc->affected_rows) {
						echo 'Send request';
					} else {
						echo "Error";
					}
				}else {
					$sql_confirm_request = 'UPDATE `friends` SET `friendship_status` = 1 WHERE `userid_x` = ' . $_SESSION['user_id'] . ' AND `userid_y` = ' . $profile_userid . ' AND `friendship_status` = 0 OR `userid_x` = ' . $profile_userid . ' AND `userid_y` = ' . $_SESSION['user_id'] . ' AND `friendship_status` = 0';

					$confirm_request_query = $dbc->query($sql_confirm_request);

					if($dbc->affected_rows) {
						echo 'confirmed';
					} else {
						echo "Error";
					}

				}
			}elseif(strcmp($friendship_status, 'no_record')===0) {
				$sql_send_request = 'INSERT INTO `friends` (`id`, `userid_x`, `userid_y`, `friendship_status`) VALUES (NULL, ' . $_SESSION['user_id'] . ', ' . $profile_userid . ', 0)';
					
				$send_request_query = $dbc->query($sql_send_request);

				if($dbc->affected_rows) {
					echo 'Cancel request';
				}else {
					echo "Error";
				}

			}else {
				echo "Error";
			}
		}

		function unfriend($profile_userid) {

			session_start();
			global $dbc;

			$profile_userid = htmlentities(strip_tags(intval($profile_userid)));

			$sql_unfriend = 'DELETE FROM `friends` WHERE `userid_x` = ' . $_SESSION['user_id'] . ' AND `userid_y` = ' . $profile_userid . ' AND `friendship_status` = 1 OR `userid_x` = ' . $profile_userid . ' AND `userid_y` = ' . $_SESSION['user_id'] . ' AND `friendship_status` = 1';

			$dbc->query($sql_unfriend);

			if($dbc->affected_rows) {
				return true;
			} else {
				return false;
			}
		}
		
		function confirm_request($requester_id) {

			session_start();
			global $dbc;

			$requester_id = htmlentities(strip_tags(intval($requester_id)));

			$sql_confirmreq = 'UPDATE `friends` SET `friendship_status` = 1 WHERE `userid_x` = ? AND `userid_y` = ? AND `friendship_status` = 0 LIMIT 1';

			$confirmreq_stmt = $dbc->prepare($sql_confirmreq);
			$confirmreq_stmt->bind_param('ii', $requester_id, $_SESSION['user_id']);
			$confirmreq_stmt->execute();

			if($confirmreq_stmt->affected_rows) {
				return true;
			}else { return false; }
		}

		function delete_request($requester_id) {

			session_start();
			global $dbc;

			$requester_id = htmlentities(strip_tags(intval($requester_id)));

			$sql_delreq = 'DELETE FROM `friends` WHERE `userid_x` = ? AND `userid_y` = ? AND `friendship_status` = 0 LIMIT 1';

			$delreq_stmt = $dbc->prepare($sql_delreq);
			$delreq_stmt->bind_param('ii', $requester_id, $_SESSION['user_id']);
			$delreq_stmt->execute();

			if($delreq_stmt->affected_rows) {
				return true;
			}else { return false; }
		}
	}
?>