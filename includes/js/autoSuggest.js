
function autoSuggest() {

	var userInput = $('#filename_search').val();
	var path = $('#path').val();

	if(!userInput) {
		$('#results').html('');
	}

	if(userInput!='') {
		$.ajax({
			url: path + 'includes/php/autosuggest.php?query=' + userInput,
			success: function(result) {
				$('#results').html(result);
			}
		});
	}
}