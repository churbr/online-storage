function user_search() {

	var key = $('#user_search').val();
	var path = $('#path').val();

	if(!key) {
		$('#user_search_results').html('');
	}

	if(key!='') {
		$.ajax({
			url: path + 'includes/php/ajax_user_search.php?user_search_key=' + key,
			success: function(result) {
				$('#user_search_results').html(result);
			}
		});
	}
}