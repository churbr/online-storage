$(document).ready(function(){

	$('#signup_form').on('submit', function() {
		var this_form = $(this);

		var action = 'includes/php/' + this_form.attr('action') + '.php';
		var method = this_form.attr('method');
		var data = {};

		this_form.find('[name]').each(function() {
			var this_attribute = $(this);

			var name = this_attribute.attr('name');
			var value = this_attribute.val();

			if(name!='gender') {
				data[name] = value;
			}
		});

		data.gender = $('#gender_field:checked').val();

		$.ajax({
			url: action,
			type: method,
			data: data,
			success: function(result) {
				if(result=='success') {
					var message = 'You have successfully registered. You will now be redirected to the main page.';
					
					$('#registration_info').html("<p class='registration_success'>" + message + "</p>");
					$('input, select').attr('disabled', 'disabled');
					setTimeout(function(){ window.location.replace('/') }, 8000);

				}else{ $('#registration_info').html(result); }
			}
		});

		return false;
	});

	
	$('#show_login_form').click(function(){
		$('#login').fadeIn(500);
	});

	$('#userlogin_form').on('submit', function() {
		var this_form = $(this);
		var path = $('#path').val();

		var action = path + 'includes/php/' + this_form.attr('action') + '.php';
		var method = this_form.attr('method');
		var data = {};

		this_form.find('[name]').each(function() {
			var this_attribute = $(this);

			var name = this_attribute.attr('name');
			var value = this_attribute.val();
			data[name] = value;
		});

		$.ajax({
			url: action,
			type: method,
			data: data,
			success: function(result) {
				if(result=='success') {
					window.location.replace('/profile');
				}else {
					$('#userlogin_msg').html(result);
				}
			}
		});

		return false;
	});

	$('#main_searchbar, #sub_searchbar').on('submit', function(e) {
		var divName = $(e.target).attr('id');

		if(divName=='main_searchbar') {
			var main_searchfield = $('#filename_search').val().length;
			if(main_searchfield===0) {
				return false;
			}
		}else {
			var sub_searchfield = $('#filename_search_result').val().length;
			if(sub_searchfield===0) {
				return false;
			}
		}
	});

	$('#suggestions_link').click(function() {
		$('#suggestions_div').fadeIn(400);
		$('body').append("<div id='background'></div>");
		$('#background').fadeIn(400);
	});

	$('a.close').click(function() {
		$('#background').remove();
		$('#suggestions_div').fadeOut(100);
	});

	$(document).keyup(function(e) {
		if(e.keyCode == 27) {
			$('#background').remove();
			$('#suggestions_div').fadeOut(100);
		}
	});

	$('#suggestion_form').on('submit', function() {

		var this_form = $(this);
		var action = this_form.attr('action');
		var method = this_form.attr('method');

		var fullname = $('#fullname').val();
		var email = $('#email').val();
		var suggestion = $('#suggestion').val();

		$.ajax({
			url: action,
			type: method,
			data: { name: $('#fullname').val(), email: $('#email').val(), file_id: $('#sugg_file_id').attr('value'), suggestion:  $('#suggestion').val(), suggested: true },
			success: function(result) {
				if(result == 'success') {
					$('#fullname').val('');
					$('#email').val('');
					$('#suggestion').val('');
					
					$('#background').remove();
					$('#suggestions_div').fadeOut(100);
				}else{
					$('#suggestion_msg').html(result);
				}
			}
		});

		return false;
	});

	$('#forgotpass_form').on('submit', function() {

		$.ajax({
			url: 'includes/php/password_reset.php',
			type: 'POST',
			data: { username: $('#fp_username').val(), fp_username_sent: true },
			success: function(result) {
				var status = result.substr(10,5);

				if(status!='error') {
					$('#fp_form_wrapper').html(result);
				}else {
					$('#fp_msg').html(result);
				}
			}
		});

		return false;
	});

	$('#forgotpass_second_form').on('submit', function() {

		$.ajax({
			url: 'includes/php/password_reset.php',
			type: 'POST',
			data: { user_id: $('#user_id').val(), username: $('#username').val(), answer: $('#forgotpass_answer').val(), fp_answer_sent: true },
			success: function(result) {
				var status = result.substr(10, 5);

				if(status!='error') {
					$('#fp_form_wrapper').html(result);
				}else {
					$('#fp_msg').html(result);
				}
			}
		});

		return false;
	});

	$('#resetpass_form').on('submit', function() {
		var user_id = $('#user_id').val();
		var username = $('#username').val();
		var answer = $('#answer').val();
		var newpass = $('#newpass').val();
		var confirmpass = $('#confirmpass').val();

		$.ajax({
			url: 'includes/php/password_reset.php',
			type: 'POST',
			data: { user_id: user_id, username: username, answer: answer, newpass: newpass, confirmpass: confirmpass, password_reset: true },
			success: function(result) {
				var status = result.substr(10, 5);

				if(status!='error') {
					$('#fp_form_wrapper').html("<p class='registration_success' style='margin-top: 8px;'> You have successfully changed your password. You can now login. </p>");
					setTimeout(function(){ window.location.replace('/') }, 8000);
				}else {
					$('#fp_msg').html(result);
				}
			}
		});

		return false;
	});
	
    $(window).on('keydown', function(e) {
    	var key = e.keyCode;
    	var FileSearchResultIsShown = $('#results ul').is(':visible');

    	if(key === 27 && FileSearchResultIsShown) {
    		$('#results').html('');
    		$('#filename_search').val('');
    	}
    });
    
});