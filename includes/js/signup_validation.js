function signup_form(id) {
	var field = $(id).attr('name');

	switch(field) {
		case 'firstname':
			setTimeout(check_firstname, 3000);
		break;

		case 'lastname':
			setTimeout(check_lastname, 3000);
		break;

		case 'username':
			setTimeout(check_username, 3000);
		break;

		case 'password':
			setTimeout(check_password, 3000);
		break;

		case 'repassword':
			setTimeout(check_repassword, 3000);
		break;

		case 'gender':
			setTimeout(check_gender, 3000);
		break;

		case 'email':
			setTimeout(check_email, 3000);
		break;

		case 'question':
			setTimeout(check_question, 3000);
		break;

		case 'answer':
			setTimeout(check_answer, 3000);
		break;

		default:
			console.log('Problem.');
		break;
	}
}

function check_firstname() {
	$.post(
		'includes/php/signup_validation.php',
		{ firstname: $('#fname_field').val(), field: 'firstname' },
		function(result) {
			$('#fname_span').html(result);
		}
	);
}

function check_lastname() {
	$.post(
		'includes/php/signup_validation.php',
		{ lastname: $('#lname_field').val(), field: 'lastname' },
		function(result) {
			$('#lname_span').html(result);
		}
	);
}

function check_username() {
	$.post(
		'includes/php/signup_validation.php',
		{username: $('#username_field').val(), field: 'username' },
		function(result) {
			$('#username_info').html(result);
		}
	);
}

function check_password() {
	$.post(
		'includes/php/signup_validation.php',
		{ password: $('#password_field').val(), field: 'password' },
		function(result) {
			$('#password_info').html(result);
		}
	);
}

function check_repassword() {
	$.post(
		'includes/php/signup_validation.php',
		{ password: $('#password_field').val(), repassword: $('#repassword_field').val(), field: 'repassword' },
		function(result) {
			$('#repassword_info').html(result);
		}
	);
}

function check_gender() {
	$.post(
		'includes/php/signup_validation.php',
		{ gender: $('#gender_field:checked').val(), field: 'gender' },
		function(result) {
			$('#gender_info').html(result);
		}
	);
}

function check_email() {
	$.post(
		'includes/php/signup_validation.php',
		{ email: $('#email_field').val(), field: 'email' },
		function(result) {
			$('#email_info').html(result);
		}
	);
}

function check_question() {
	$.post(
		'includes/php/signup_validation.php',
		{ question:	$('#question_field option:selected').text(), field: 'question' },
		function(result) {
			$('#secretquestion_info').html(result);
		}
	);
}

function check_answer() {
	$.post(
		'includes/php/signup_validation.php',
		{ answer: $('#answer_field').val(), field: 'answer' },
		function(result) {
			$('#secretanswer_info').html(result);
		}
	);
}