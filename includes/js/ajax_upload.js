var handleUpload = function(event) {
	event.preventDefault();
	event.stopPropagation();

	var the_file = document.getElementById('the_file');

	var request = new XMLHttpRequest();
	var data = new FormData();

	if(the_file.files.length) {

		for(var i = 0; i <= the_file.files.length; ++i) {
			data.append('file[]', the_file.files[i]);
		}

		/* ------------------------------------------------------------------------- */

		request.upload.addEventListener('progress', function(e) { // while in process
			if(e.lengthComputable) {
				var percent = e.loaded / e.total;

				$('#file_upload input').attr('disabled', 'disabled');
				$('#upload_progressbar').progressbar({ value: Math.round(percent * 100) });
				$('#percent_info').html(Math.round(percent * 100) + '%');
			}
		});

		/* ------------------------------------------------------------------------- */

		request.upload.addEventListener('load', function(e) { // When it's done
			$('#percent_info').html('Finishing...');
			setTimeout(function() {
				$('#file_upload input').removeAttr('disabled');
				document.getElementById('upload_progressbar').style.display = 'none';
				document.getElementById('percent_info').style.display = 'none';
			}, 3000);
		});

		/* ------------------------------------------------------------------------- */

		request.upload.addEventListener('error', function(e) { // When there's a problem
			alert('Upload failed.');
		});

		/* ------------------------------------------------------------------------- */

		request.addEventListener('readystatechange', function(e) {

			if(this.readyState == 4) { // 4 means request is finished and response is ready
				if(this.status == 200) { // 200 is the HTTP status code for OK
					var uploaded = eval(this.response);

					setTimeout(function() {
						for (var i = 0; i < uploaded.length; ++i) {
							$('#view_uploaded_files ul').append(uploaded[i]);
						}

						$('#the_file').val('');
						$('#upload_button').val('Add More Files...');

					}, 2000);

				}else {
					console.log('Server replied with HTTP status: ' + this.status);
				}
			}
		});

		/* ------------------------------------------------------------------------- */

		data.append('file_uploaded', true); // extra POST value just to confirm that data is sent via ajax

		request.open('POST', 'includes/php/ajax_upload.php');
		request.setRequestHeader('Cache-Control', 'no-cache');

		document.getElementById('upload_progressbar').style.display = 'block';
		document.getElementById('percent_info').style.display = 'block';
		request.send(data);

	}else {
		$('#file_upload_message').html("<p class='error'>Please choose a file.</p>").fadeOut(5000);
	}
}


window.addEventListener('load', function(event) {
	var submit = document.getElementById('upload_button');
	submit.addEventListener('click', handleUpload);
});