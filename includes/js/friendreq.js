function confirm_friendreq(id) {

	$('.confirm_button_' + id).css('background', '#404040');
	$('.confirm_button_' + id).attr('disabled', 'disabled');
	$('.delete_button_' + id).attr('disabled', 'disabled');

	$.ajax({
		url: 'includes/php/friendreq.php',
		type: 'POST',
		data: { requester_id : id, action: 'confirmed' },
		success: function (result) {
			if(result == 'success') {
				$('#thumbnail_container_' + id).fadeOut(400);
			}
		}
	});

	return false;
}

function delete_friendreq(id) {

	$('.delete_button_' + id).css('background', '#404040');
	$('.delete_button_' + id).attr('disabled', 'disabled');
	$('.confirm_button_' + id).attr('disabled', 'disabled');

	$.ajax({
		url: 'includes/php/friendreq.php',
		type: 'POST',
		data: { requester_id : id, action: 'deleted' },
		success: function (result) {
			if(result == 'success') {
				$('#thumbnail_container_' + id).fadeOut(400);
			}
		}
	});

	return false;
}