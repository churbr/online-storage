$(document).ready(function() {

	$('.user_basic_info').hide();

	$('.options').on('click', function() {
		var state = $('#options').is(':visible');

		if(state) {
			$('#options').hide();
		}else{
			$('#options').show();
		}
	});

	$('#upload_button').click(function() {
		$('#upload_progressbar').progressbar();
		return false;
	});

   $('.view_user_info').click(function () {
   		$('.user_basic_info').show(200);
   		$('.user_files_menu').hide(200);
    });

	$('.view_user_files').click(function () {
		$('.user_files_menu').show(200);
   		$('.user_basic_info').hide(200);
    });

    $(window).on('keydown', function(e) {
    	var key = e.keyCode;
    	var userSearchResultIsShown = $('#user_search_results ul').is(':visible');

    	if(key === 27 && userSearchResultIsShown) {
    		$('#user_search_results ul').html('');
    		$('#user_search').val('');
    	}
    });

    $('#user_edit_file .close').on('click', function() {
    	$('#user_edit_file').remove();
    });

    $('#edit_music_fileinfo .close').on('click', function() {
      $('#edit_music_fileinfo').remove();
    });

    $('#edit_file_info').on('submit', function() {
      var id = $('#id').val();
      var name = $('#filename').val();
      var desc = $('#filedesc').val();
      var share = $('#sharedto').val();
      var category = $('#moveto').val();

      $.ajax({
        url: 'includes/php/profile_loggedin_functions.php',
        type: 'POST',
        data: { file_id : id, file_name : name, file_desc : desc, file_share : share, file_category : category, file_info_edited: true },
        success: function(response) {

          if(response == 'success') {
            var path = $(location).attr('pathname');
            $('#edit_file_info_msg').html("<p class='success'>File Info successfully updated.</p>");
            setTimeout(function(){ window.location.replace(path) }, 3000);
          }else {
            $('#edit_file_info_msg').html(response);
          }
          
        }
      });

      return false;
    });

    $('#request_button').on('click', function() {

      var profile_userid = $('#profile_userid').val();
      var redirectUrl = $(location).attr('pathname');
      var path = $('#path').val();

      $.ajax({
        url: path + 'includes/php/profile_loggedin_functions.php',
        type: 'POST',
        data: { profile_userid : profile_userid, request_button_click : true },
        success: function(response) {
          if(response == 'confirmed') {
            window.location.replace(redirectUrl);
          }else {
            $('#request_button span').text(response);
          }
        }
      });
    });

    $('#unfriend').on('click', function() {
      var profile_userid = $('#profile_userid').val();
      var redirectUrl = $(location).attr('pathname');
      var path = $('#path').val();

      $.ajax({
        url: path + 'includes/php/profile_loggedin_functions.php',
        type: 'POST',
        data: { profile_userid : profile_userid, unfriended : true },
        success: function(result) {
          if(result=='success') {
            window.location.replace(redirectUrl);
          } else {
            alert('Something went wrong.');
         }
        }
      });

    });


    $('.user_detail button').mouseenter(

      function() {
        var this_button = $(this);
        var id = this_button.attr('class');
        $('.user_detail button.' + id).css('border', '1px solid #70E0FF');
        $('.user_detail button.' + id).css('box-shadow', '1px 1px 3px #70E0FF');
        $('.user_detail button.' + id).text('Unfriend');
      }
    ).mouseleave(
      function() {
        var this_button = $(this);
        var id = this_button.attr('class');
        $('.user_detail button.' + id).css('border', '1px solid #ddd');
        $('.user_detail button.' + id).css('box-shadow', '1px 1px 3px #ddd');
        $('.user_detail button.' + id).text('Friends');
      }
    );

    $('.user_detail button').on('click', function(evnt) {

      var this_button = $(this);
      var profile_userid = this_button.attr('class');
      var path = $('#path').val();

      $.ajax({
        url: path + 'includes/php/profile_loggedin_functions.php',
        type: 'POST',
        data: { profile_userid : profile_userid, unfriended : true },
        success: function(result) {
          if(result== 'success') {
            $('#friends_list td.' + profile_userid).fadeOut(400);
          }
        }
      });
      
      evnt.stopPropagation();
      evnt.preventDefault();
      return false;
    });
    
});