<?php
	require '../classes/hydra_user.class.inc';
	require 'config.inc';
	$user = new User;

	if(isset($_POST['fp_username_sent'])) {

		$username = htmlentities(strip_tags(addslashes($_POST['username'])));

		if(!empty($username)) {
			if($user->username_exists($username)) {
				$user_id = $user->get_userid($username);

				$fp_sql = "SELECT `question` FROM `verification` WHERE `user_id` = $user_id";
				
				$query = $dbc->query($fp_sql);
				$fetched = $query->fetch_object();
				$question = $fetched->question;
				$script = BASE_URL . 'includes/js/functions.js';

				echo <<<SECOND_FP_FORM
					<form id='forgotpass_second_form'>
						<input type='hidden' id='user_id' value='$user_id' />
						<input type='hidden' id='username' value='$username' />
						<p> $question </p>
						<p> <input type='password' id='forgotpass_answer' placeholder='Your secret answer...' /> </p>
						<p> <input class='fp_submit_button' type='submit' value='Reset Password' /> </p>
					</form>
					<div id='fp_msg'></div>
					<script src='$script'></script>
SECOND_FP_FORM;

			$query->close();

			}else {
				echo "<p class='error'>Username does not exist.</p>";
			}
		}else {
			echo "<p class='error'>Please input something.</p>";
		}
	}


	if(isset($_POST['fp_answer_sent'])) {

		$user_id = htmlentities(strip_tags(addslashes(intval($_POST['user_id']))));
		$username = htmlentities(strip_tags(addslashes($_POST['username'])));
		$answer = htmlentities(strip_tags(addslashes(strtolower($_POST['answer']))));

		if(!empty($answer)) {
			if($user_id!==0) {

				$fetch_answer_sql = "
					SELECT `users`.`user_id`, `users`.`username`, `verification`.`answer`
					FROM `verification` JOIN `users`
					ON `users`.`user_id` = `verification`.`user_id`
					WHERE `verification`.`user_id` = $user_id AND `users`.`username` = '$username'
				";

				$query = $dbc->query($fetch_answer_sql);

				if($query->num_rows) {
					$fetch = $query->fetch_object();

					$db_userid = $fetch->user_id;
					$db_username = $fetch->username;
					$db_answer = $fetch->answer;

					$script = BASE_URL . 'includes/js/functions.js';

					if(strcmp($answer, $db_answer) === 0) {

						echo <<<THIRD_FP_FORM
							<form id='resetpass_form'>
								<input type='hidden' id='user_id' value='$db_userid' />
								<input type='hidden' id='username' value='$db_username' />
								<input type='hidden' id='answer' value='$db_answer' />
								<p> New password <input type='password' id='newpass' /> </p>
								<p> Confirm Password <input type='password' id='confirmpass' /> </p>
								<p> <input type='submit' class='fp_submit_button' value='Change Password' /> </p>
							</form>
							<div id='fp_msg'></div>
							<script src='$script'></script>
THIRD_FP_FORM;

					$query->close();

					}else { echo "<p class='error'>Incorrect answer.</p>"; }
				}else { echo "<p class='error'>No records that corresponds to that detail.</p>"; }
			}else { echo "<p class='error'>Invalid ID</p>"; }
		}else { echo "<p class='error'>Answer is required.</p>"; }
	}


	if(isset($_POST['password_reset'])) {

		if(!empty($_POST['newpass']) && !empty($_POST['confirmpass'])) {
    		$user_id = htmlentities(strip_tags(addslashes(intval($_POST['user_id']))));
    		$username = htmlentities(strip_tags(addslashes($_POST['username'])));
    		$answer = htmlentities(strip_tags(addslashes($_POST['answer'])));
    		$newpass = htmlentities(strip_tags(addslashes(strtolower($_POST['newpass']))));
    		$confirmpass = htmlentities(strip_tags(addslashes(strtolower($_POST['confirmpass']))));

    		if($user->check_password($newpass)) {
    			if(strcmp($newpass, $confirmpass)===0) {

    				$data_integrity_sql = "
						SELECT `users`.`username`, `verification`.`answer`
						FROM `users` JOIN `verification`
						ON `users`.`user_id` = `verification`.`user_id`
						WHERE `users`.`user_id` = $user_id AND `users`.`username` = '$username' AND `verification`.`answer` = '$answer'
    				";

    				$query = $dbc->query($data_integrity_sql);


    				if($query->num_rows) {
    					$newpass = md5($newpass);

    					$statement = $dbc->prepare("UPDATE `users` SET `password` = '$newpass' WHERE `user_id` = ? AND `username` = ?");
    					$statement->bind_param('is', $user_id, $username);
    					$statement->execute();

    					if($dbc->affected_rows) {
    						echo 'success';
    					}else {
    						echo "<p class='error'>The new password you set is the same with the old password.</p>";
    					}

    				}else {
    					echo "<p class='error'>Fabricated.</p>";
    				}

    				$query->close();
    				$statement->close();
    				$dbc->close();


    			}else { echo "<p class='error'>Passwords do not match.</p>"; }
    		}else { echo "<p class='error'>Password must be atleast 8 characters long.</p>"; }
		}else { echo "<p class='error'>Please complete the fields.</p>"; }
	}
?>