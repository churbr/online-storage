<?php
	require '../classes/hydra_user.class.inc';
	$user = new User;

	if(isset($_POST['loggedin'])) {
		if(!empty($_POST['username']) && !empty($_POST['password']) ) {

			$username = htmlentities(strip_tags($_POST['username']));
			$password = htmlentities(strip_tags($_POST['password']));

			if($user->username_exists($username)) {
				$user_id = $user->get_userid($username);

				if(!$user->user_blocked($user_id)) {
					if($user->db_password_matched($username, $password)) {
						$user->establish_session($username);
						echo 'success';
					}else {
						echo "<p class='incorrect'>Incorrect password.</p>";
					}
				}else {
					echo "<p class='error'>ACCOUNT BLOCKED</p>";
				}

			}else {
				echo "<p class='incorrect'>Username doesn't exist.</p>";
			}
		}else{
			echo "<p class='incorrect'>Please complete the form to login.</p>";
		}
	}
?>