<?php
	session_start();
	
	require '../classes/hydra_upload_framework.class.inc';
	require '../classes/hydra_user.class.inc';
	require 'db.inc';

	$edit_help = new file_upload;
	$user = new User;

	if(isset($_POST['file_info_edited'])) {

		$editfile_msg = '';
		$problem = false;

		$file_id = intval($_POST['file_id']);
		$filename = htmlentities(strip_tags($_POST['file_name']));
		$filedesc = htmlentities(strip_tags($_POST['file_desc']));
		$fileshare = intval($_POST['file_share']);
		$filecategory = htmlentities(strip_tags($_POST['file_category']));

		if(!$edit_help->check_filename($filename)) {
			$problem = true;
			$editfile_msg .= '<p class="error">Filename is incorrect format.</p>';
		}

		if(!$edit_help->check_fileshare($fileshare)) {
			$problem = true;
			$editfile_msg .= '<p class="error">Privacy value is invalid.</p>';
		}

		if(!$edit_help->check_filecategory($filecategory)) {
			$problem = true;
			$editfile_msg .= '<p class="error">File category is invalid.</p>';
		}

		if(!$problem) {

			$filename = $dbc->escape_string($filename);
			$filedesc = $dbc->escape_string($filedesc);
			$filecategory = $dbc->escape_string($filecategory);

			$sql_update_file = "UPDATE `files` SET `file_name` = '$filename', `file_desc` = '$filedesc', `file_accessibility` = $fileshare, `file_category` = '$filecategory' WHERE `file_id` = $file_id AND `user_id` = " . $_SESSION['user_id'];

			$update_fileinfo_query = $dbc->query($sql_update_file);

			if($dbc->affected_rows) {
				$editfile_msg = 'success';
			}else {
				$editfile_msg = "<p class='error'>Nothing is changed.</p>";
			}
		}

		echo $editfile_msg;
	}

	if(isset($_POST['request_button_click'])) {
		$profile_userid = intval($_POST['profile_userid']);
		$user->send_request($profile_userid);
	}

	if(isset($_POST['unfriended'])) {
		$profile_userid = $_POST['profile_userid'];

		$result = $user->unfriend($profile_userid);

		if($result) {
			echo 'success';
		}else {
			echo 'error';
		}
	}
	
?>