<?php
	session_start();
	require 'config.inc';
	require DB;
	require BASE_URI . 'includes/classes/hydra_upload_framework.class.inc';
	require BASE_URI . 'includes/classes/hydra_file_framework.class.inc';

	$upload_file = new file_upload;
	$upload_help = new hydra_file_framework;

	if(isset($_POST['file_uploaded'])) {

		$file_count = count($_FILES['file']['name']);
		$fileicon_array = array();
		$filenames_array = array();
		$filebin_array = array();
		$filesize_array = array();
		$filebytesize_array = array();
		$filetype_array = array();
		$filecategory_array = array();
		$filedestinationfolder_array = array();
		$filelinkdestination_array = array();

		foreach ($_FILES['file']['name'] as $filename_index => $filename) {
			$filename = strtolower($filename);

			array_push($fileicon_array, $upload_help->file_icon($upload_help->file_category(htmlentities(strip_tags(end(explode('.', $filename)))))));
			array_push($filenames_array, $upload_file->get_filename($filename));
			array_push($filebin_array, $upload_file->make_random_filename($filename));
			array_push($filetype_array, htmlentities(strip_tags(end(explode('.', $filename)))));
			array_push($filecategory_array, $upload_help->file_category(htmlentities(strip_tags(end(explode('.', $filename))))));
			array_push($filedestinationfolder_array, $upload_file->select_folder($upload_help->file_category(htmlentities(strip_tags(end(explode('.', $filename)))))));
			array_push($filelinkdestination_array,  $upload_file->select_link($upload_help->file_category(htmlentities(strip_tags(end(explode('.', $filename)))))));
		}

		foreach ($_FILES['file']['size'] as $filesize_index => $filesize) {
			array_push($filesize_array, $upload_file->format_file_size($filesize));
			array_push($filebytesize_array, $filesize);
		}

		$upload_sql = 'INSERT INTO `files` (`file_id`, `user_id`, `file_name`, `file_desc`, `file_bin`, `file_size`, `file_accessibility`, `file_category`, `file_type`, `file_downloaded`, `date_uploaded`) VALUES ';

		for($i = 0; $i < $file_count; $i++) {
			$user_id = $_SESSION['user_id'];
			$file_name = $dbc->escape_string($filenames_array[$i]);
			$file_bin = $dbc->escape_string($filebin_array[$i]);
			$file_size = $dbc->escape_string($filesize_array[$i]);
			$filebyte_size = $filebytesize_array[$i];
			$file_category = $dbc->escape_string($filecategory_array[$i]);
			$file_type = $dbc->escape_string($filetype_array[$i]);

			if(intval($filebyte_size) <= 104857600) {
				$upload_sql .= "(NULL, '$user_id', '$file_name', NULL, '$file_bin', '$file_size', '1', '$file_category', '$file_type', '0', NOW()), ";
			}
		}

		$upload_sql = substr($upload_sql, 0, -2) . ';';

		/* Move file to server directory */

		foreach ($_FILES['file']['name'] as $file_index => $file_name) {
			if(intval($_FILES['file']['size'][$file_index]) <= 104857600 && $_FILES['file']['error'][$file_index] == 0) {
				if(move_uploaded_file($_FILES['file']['tmp_name'][$file_index], '../../files/' . $filedestinationfolder_array[$file_index] . '/' . $filebin_array[$file_index])) {
					$uploaded[] = "
						<li>
							<a href='/{$filelinkdestination_array[$file_index]}'>
								<p> <img src='images/resource/{$fileicon_array[$file_index]}' /> <span class='filename'> ({$filecategory_array[$file_index]}) {$filenames_array[$file_index]} </span> <span class='filesize'>{$filesize_array[$file_index]}</span> <span class='filetype'> {$filetype_array[$file_index]} File</span> </p>
							</a>
						</li>
					";
					$no_problem = TRUE;
				}else {
					$no_problem = FALSE;
				}
			}else {
				$uploaded[] = "<li> <p class='error'> Cannot upload file " . strtolower($_FILES['file']['name'][$file_index]) . " because file size is too large. </p> </li>";
			}
		}

		/* Insert File Data to Database */

		if($dbc->query($upload_sql)) {
			if($dbc->affected_rows && $no_problem) {
				$uploaded[] = "<p class='success'>File successfully uploaded.</p>";
			}else {
				$uploaded[] = "<p class='error'>Something went wrong when uploading the file.</p>";
			}
		}

		echo json_encode($uploaded);
	}
?>