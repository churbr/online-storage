<?php
	require 'config.inc';
	require 'db.inc';

	if(isset($_GET['user_search_key'])) {

		$key = htmlentities(strip_tags($_GET['user_search_key']));

		$sql = '
			SELECT `user_id`, `picture`, `firstname`, `lastname`
			FROM `users`
			WHERE `firstname` LIKE "%' . $dbc->escape_string($key) . '%" OR `lastname` LIKE "%' . $dbc->escape_string($key) . '%"
			LIMIT 5
		';

		$query = $dbc->query($sql);

		$response = '<ul>';

		if($query->num_rows) {

			while($data = $query->fetch_object()) {

				$user_id = $data->user_id;
				$fullname = $data->firstname . ' ' . $data->lastname;
				$picture = $data->picture;

			    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $fullname);
			    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
			    $name_link = preg_replace('/[\s_]/', '-', $name_link);
			    $name_link = strtolower($name_link);

			    $response .= '<li> <a href="' . BASE_URL . 'user/' . $user_id . '/' . $name_link . '"> <img src="' . BASE_URL . '/images/users/' . $picture . '" />' . $fullname . '</a> </li>';
			}
		}else { $response .= "<li> <p class='error'>No result.</p> </li>"; }

		$response .= '</ul>';

		echo $response;
	}
?>