<?php

	if(isset($_POST['suggested'])) {
		require_once '../classes/hydra_user.class.inc';
		$user = new User;

		$name = htmlentities(strip_tags($_POST['name']));
		$email = htmlentities(strip_tags($_POST['email']));
		$file_id = htmlentities(strip_tags($_POST['file_id']));
		$suggestion = htmlentities(strip_tags($_POST['suggestion']));

		if(!empty($file_id)) {

			$query = $dbc->query("SELECT `file_name` FROM `files` WHERE `file_id` = $file_id");

			if($num_rows = $query->num_rows) {

				$name_check = $user->validate_name($name);
				$email_check = $user->validate_email($email);

				if($name_check&&$email_check) {
					if(!empty($suggestion) && (strlen($suggestion) >= 10) ) {
						$name = ucfirst(strtolower($dbc->escape_string($name)));
						$email = strtolower($dbc->escape_string($email));
						$suggestion = ucfirst(strtolower($dbc->escape_string($suggestion)));

						$add_suggestion_sql = "
							INSERT INTO `suggestions` (`id`, `file_id`, `fullname`, `email`, `message`, `date_received`)
							VALUES (NULL, '$file_id', '$name', '$email', '$suggestion', NOW())
						";

						$query = $dbc->query($add_suggestion_sql);

						if($dbc->affected_rows) {
							echo 'success';
						}else {
							echo "<p class='incorrect'>Sorry, we're having some problems.</p>";
						}
					}else {
						echo "<p class='incorrect'>Your suggestion is too short.</p>";
					}
				}else {
					echo "<p class='incorrect'>Name or email is invalid.</p>";
				}
			}else {
				echo "<p class='incorrect'>This file does not exist.</p>";
			}
		}
	}

	function autoSuggest($query) {

		require_once 'db.inc';
		require_once 'config.inc';
		require_once '../classes/hydra_file_framework.class.inc';

		$file = new hydra_file_framework;

		$sql = '
			SELECT `file_id`, `file_name`, `file_bin`, `file_accessibility`, `file_size`, `file_category`, `file_type`
			FROM `files`
			WHERE
			`file_accessibility` = 1 AND 
			`file_name` LIKE "%' . $dbc->escape_string($query) . '%" OR
			`file_bin` LIKE "%' . $dbc->escape_string($query) . '%"
			LIMIT 8
		';

		$query = $dbc->query($sql);

		if($query->num_rows) {

			$result = '<ul>';

			while($data = $query->fetch_object()) {
				$id 		= $data->file_id;
				$name 		= $data->file_name;
				$bin 		= $data->file_bin;
				$access 	= $data->file_accessibility;
				$size 		= $data->file_size;
				$category 	= $data->file_category;
				$type 		= $data->file_type;
				$icon		= $file->file_icon($category);

				if(intval($access) === 1) {

					/* Clean URL filename */

				    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $name);
				    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
				    $name_link = preg_replace('/[\s_]/', '-', $name_link);

					$result .=	'<form action="" method="POST">';
					$result .=	'<li><p><img src="' . BASE_URL . 'images/resource/' . $icon . '" />';
					$result .=	'<span class="filename">' . $name . '</span>';
					$result .=	'<span class="filetype">' . $type . ' File</span>';
					$result .=	'<span class="filesize">' . $size . '</span>';
					$result .=	'<a href="view/' . $id . '/' . $name_link . '/">View</a>';
					$result .=	'<input type="hidden" name="file_id" value=' . $id . ' />';
					$result .=	'<input type="hidden" name="downloaded" value="true" />';
					$result .=	'<input type="submit" value="Download" />';
					$result .=	'</p></li></form>';
				}
			}

			$result .= '</ul>';
		}else {
			$result = '<p class="error">No match found.</p>';
		}

		echo $result;
	}

	function searched_results($query) {
		require DB;
		require_once BASE_URI . 'includes/classes/hydra_file_framework.class.inc';
		$query = preg_replace('/[-]/', ' ', $_GET['query']);
		$file = new hydra_file_framework;

		$sql = '
			SELECT `file_id`, `file_name`, `file_bin`, `file_accessibility`, `file_size`, `file_category`, `file_type`
			FROM `files`
			WHERE
			`file_name` LIKE "%' . $dbc->escape_string($query) . '%" OR
			`file_bin` LIKE "%' . $dbc->escape_string($query) . '%"
		';

		$query = $dbc->query($sql);

		if($num_rows = $query->num_rows) {

			$word = ($num_rows == 1) ? 'file' : 'files';

			echo <<<NUM_RESULTS
				<div id='num_results'>$num_rows $word found.</div>
				<div id='clear'></div>
NUM_RESULTS;

			echo '<ul>';

			while($data = $query->fetch_object()) {
				$id 		= $data->file_id;
				$name 		= $data->file_name;
				$bin 		= $data->file_bin;
				$access 	= $data->file_accessibility;
				$size 		= $data->file_size;
				$category 	= $data->file_category;
				$type 		= $data->file_type;
				$icon		= $file->file_icon($category);
				$icon_path	= BASE_URL . 'images/resource/' . $icon;

				if(intval($access) == 1) {

				/* Clean URL filename */

			    $name_link = preg_replace('/[^A-Za-z0-9_\s-]/', '', $name);
			    $name_link = preg_replace('/[\s-]+/', ' ', $name_link);
			    $name_link = preg_replace('/[\s_]/', '-', $name_link);

				echo <<< SEARCHED_RESULT
					<form action='' method='POST'>
						<li>
							<p>
								<img src='$icon_path' />
								<span class="searched_filename"> $name </span>
								<span class="searched_filetype">$type File</span>
								<span class="searched_filesize">$size</span>
								<a href="view/$id/$name_link/">View</a>
								<input type="hidden" name="file_id" value='$id' />
								<input type="hidden" name="downloaded" value="true" />
								<input type="submit" value="Download" />
								<div id='clear'></div>
							</p>
						</li>
					</form>
					<div id='clear'></div>
SEARCHED_RESULT;

				}

			}

			echo '</ul>';
		}else {
			echo '<p class="error">No match found.</p>';
		}
	}
?>