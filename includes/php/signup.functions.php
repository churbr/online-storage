<?php

	/* @@@@@@@@@@@@@@@@@@@@@@ ON KEY PRESS @@@@@@@@@@@@@@@@@@@@@@ */

	function process_input_firstname($firstname) {

		global $user;
		$firstname = trim(preg_replace('#\s,#', '', $firstname));

		if(!empty($firstname)) {
			if(!$user->validate_name($firstname)) {
				echo "<span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Firstname is invalid. </span> ";
			}
		}else {
			echo "<span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Please input your firstname. </span> ";
		}
	}

	function process_input_lastname($lastname) {

		global $user;
		$lastname = trim(preg_replace('#\s,#', '', $lastname));

		if(!empty($lastname)) {
			if(!$user->validate_name($lastname)) {
				echo "<span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Lastname is invalid. </span> ";
			}
		}else {
			echo "<span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Please input your lastname. </span> ";
		}
	}

	function process_input_username($username) {
		
		global $user;
		$username = trim($username);

		if(!empty($username)) {
			if($user->validate_username($username)) {
				if($user->username_exists($username)) {
					echo "
					<td></td>
					<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Username is already taken. </span> </td>
				";
				}
			}else {
				echo "
					<td></td>
					<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Invalid format/character length problems. </span> </td>
			";
			}
		}else {
			echo "
				<td></td>
				<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Please input your username. </span> </td>
			";
		}
	}

	function process_input_password($password) {
		
		global $user;

		if(!empty($password)) {
			if(!$user->check_password($password)) {
				echo "
					<td></td>
					<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Password must be atleast 8 characters long. </span> </td>
				";
			}
		}else {
			echo "
				<td></td>
				<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Please input your password. </span> </td>
			";
		}
	}

	function process_input_repassword($password, $confirm_password) {
		if(!empty($password)) {
			if(!empty($confirm_password)) {
				if(strcmp($password, $confirm_password)!==0) {
					echo "
						<td></td>
						<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Passwords do not match. </span> </td>
					";
				}
			}else {
				echo "
					<td></td>
					<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Please confirm your password. </span> </td>
				";
			}
		}else {
			echo "
				<td></td>
				<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> First password field is empty. </span> </td>
			";
		}
	}

	function process_selected_gender($gender) {

		global $user;

		if(!empty($gender)) {
			if(!$user->validate_gender($gender)) {
				echo "
					<td></td>
					<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Not one of the choices. </span> </td>
				";
			}
		}else {
			echo "
				<td></td>
				<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> No gender is selected. </span> </td>
			";
		}
	}

	function process_input_email($email) {

		global $user;
		$email = trim($email);

		if(!empty($email)) {
			if($user->validate_email($email)) {
				if($user->email_exists($email)) {
					echo "
						<td></td>
						<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Email already exist. </span> </td>
					";
				}
			}else {
				echo "
					<td></td>
					<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Email is invalid. </span> </td>
				";
			}
		}else {
			echo "
				<td></td>
				<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Please input your email. </span> </td>
			";
		}
	}

	function process_selected_question($question) {
		global $user;

		if(!$user->validate_questions($question)) {
			echo "
				<td></td>
				<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Question is invalid. </span> </td>
			";
		}
	}

	function process_input_answer($answer) {
		global $user;
		$answer = trim($answer);

		if(!empty($answer)) {
			if(!$user->validate_answer($answer)) {
				echo "
					<td></td>
					<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Answer should be atleast 3 characters long. </span> </td>
				";
			}
		}else{
			echo "
				<td></td>
				<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Please input your answer. </span> </td>
			";
		}
	}

	/* @@@@@@@@@@@@@@@@@@@@@@ ON SUBMIT @@@@@@@@@@@@@@@@@@@@@@ */

	function check_submitted_firstname($firstname) {

    	$status = TRUE;
        global $user;

        $firstname = trim(preg_replace('#\s,#', '', $firstname));

        if(!empty($firstname)) {
            if(!$user->validate_name($firstname)) { $status = FALSE; }
        }else {
        	$status = FALSE;
        }

        return $status;
    }

    function check_submitted_lastname($lastname) {

    	$status = TRUE;
        global $user;
        $lastname = trim(preg_replace('#\s,#', '', $lastname));

        if(!empty($lastname)) {
            if(!$user->validate_name($lastname)) { $status = FALSE; }
        }else { $status = FALSE; }

        return $status;
    }

    function check_submitted_username($username) {
        
        $status = TRUE;
        global $user;
        $username = trim($username);

        if(!empty($username)) {
            if($user->validate_username($username)) {
                if($user->username_exists($username)) { $status = FALSE; }
            }else { $status = FALSE; }
        }else { $status = FALSE; }

        return $status;
    }

    function check_submitted_password($password) {
        
        $status = TRUE;
        global $user;

        if(!empty($password)) {
            if(!$user->check_password($password)) { $status = FALSE; }
        }else { $status = FALSE; }

        return $status;
    }

    function check_submitted_repassword($password, $confirm_password) {
    	
    	$status = TRUE;

        if(!empty($password)) {
            if(!empty($confirm_password)) {
                if(strcmp($password, $confirm_password)!==0) { $status = FALSE; }
            }else { $status = FALSE; }
        }else { $status = FALSE; }

        return $status;
    }

    function check_submitted_gender($gender) {

    	$status = TRUE;
        global $user;

        if(!empty($gender)) {
            if(!$user->validate_gender($gender)) { $status = FALSE; }
        }else { $status = FALSE; }

        return $status;
    }

    function check_submitted_email($email) {

    	$status = TRUE;
        global $user;
        $email = trim($email);

        if(!empty($email)) { 
            if($user->validate_email($email)) {
            	if($user->email_exists($email)) { $status = FALSE; }
            }else { $status = FALSE; }
        }else { $status = FALSE; }

        return $status;
    }

    function check_updated_email($email) {

    	$status = TRUE;
        global $user;
        $email = trim($email);

        if(!empty($email)) { 
            if(!$user->validate_email($email)) { $status = FALSE; }
        }else { $status = FALSE; }

        return $status;
    }

    function check_submitted_question($question) {

    	$status = TRUE;
        global $user;

        if(!$user->validate_questions($question)) { $status = FALSE; }

        return $status;
    }

    function check_submitted_answer($answer) {

    	$status = TRUE;
        global $user;
        $answer = trim($answer);

        if(!empty($answer)) {
            if(!$user->validate_answer($answer)) { $status = FALSE; }
        }else{ $status = FALSE; }

        return $status;
    }

?>