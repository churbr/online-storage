<?php

	require '../classes/hydra_user.class.inc';
	$user = new User;

	if(isset($_POST['action'])) {
		if(in_array($_POST['action'], array('confirmed', 'deleted'))) {

			$action = htmlentities($_POST['action']);
			$requester_id = htmlentities(intval($_POST['requester_id']));

			switch ($action) {
				case 'confirmed':
					$confirm_result = $user->confirm_request($requester_id);
					if($confirm_result) { echo 'success'; }
				break;

				case 'deleted':
					$delete_result = $user->delete_request($requester_id);
					if($delete_result) { echo 'success'; }
				break;
				
				default:
					echo 'error';
				break;
			}

		}
	}
?>