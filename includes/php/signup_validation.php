<?php
	require '../classes/hydra_user.class.inc';
	require 'signup.functions.php';

	$user = new User();

	if(isset($_POST['signedup'])) {
		$firstname = check_submitted_firstname($_POST['firstname']);
		$lastname = check_submitted_lastname($_POST['lastname']);
		$username = check_submitted_username($_POST['username']);
		$password = check_submitted_password($_POST['password']);
		$repassword = check_submitted_repassword($_POST['password'] ,$_POST['repassword']);
		$gender = check_submitted_gender($_POST['gender']);
		$email = check_submitted_email($_POST['email']);
		$question = check_submitted_question($_POST['question']);
		$answer = check_submitted_answer($_POST['answer']);

		if($firstname&&$lastname&&$username&&$password&&$repassword&&$gender&&$email&&$question&&$answer) {
			if($user->add_new_user($_POST['firstname'], $_POST['lastname'], $_POST['username'], $_POST['password'], $_POST['gender'], $_POST['email'], $_POST['question'], $_POST['answer'])) {
				echo 'success';
			}
		}else{
			echo "<p class='registration_failed'>You have an error in your registration. Please fix your information to proceed.</p>";
		}
	}

	if(isset($_POST['field'])) {

		$field = $_POST['field'];

		switch ($field) {
			case 'firstname':
				process_input_firstname($_POST['firstname']);
			break;
			
			case 'lastname':
				process_input_lastname($_POST['lastname']);
			break;

			case 'username':
				process_input_username($_POST['username']);
			break;

			case 'password':
				process_input_password($_POST['password']);
			break;

			case 'repassword':
				process_input_repassword($_POST['password'], $_POST['repassword']);
			break;
			
			case 'gender':
				process_selected_gender($_POST['gender']);
			break;

			case 'email':
				process_input_email($_POST['email']);
			break;

			case 'question':
				process_selected_question($_POST['question']);
			break;

			case 'answer':
				process_input_answer($_POST['answer']);
			break;

			default:
					echo "
						<td></td>
						<td> <span class='incorrect'> <img src='images/resource/fail.png' style='width: 22px; height: 22px;' /> Something went wrong. </span> </td>
					";
			break;
		}
	}
?>